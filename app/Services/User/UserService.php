<?php
namespace App\Services\User;

use App\Entities\User;
use App\Event;
use App\Http\Requests\User\CreateRequest;
use App\Http\Requests\User\UpdateRequest;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Jobs\ProcessChangeUserStatus;

/**
 * Class UserService
 */
class UserService
{
    /**
     * @param User $user
     * @param int $status
     */
    public function changeStatus(User $user, int $status): void
    {
        $before_status = $user->status;
        if (!in_array($status, array_keys(User::statusesList()))) {
            throw new \DomainException('Status not allowed');
        }
        $user->update(['status' => $status]);
        if ($before_status !== User::STATUS_EMPLOYEE && $user->status === User::STATUS_EMPLOYEE)
        {
            $user->update(['employer_at' => date('Y-m-d H:i:s', time())]);
            $user->update(['fired_at' => null]);
            ProcessChangeUserStatus::dispatch($user, User::STATUS_EMPLOYEE);
        }
        if ($before_status === User::STATUS_EMPLOYEE && $user->status === User::STATUS_FIRED)
        {
            $user->update(['fired_at' => date('Y-m-d H:i:s', time())]);
            ProcessChangeUserStatus::dispatch($user, User::STATUS_FIRED);
        }
    }

    /**
     * @param UpdateRequest $request
     * @param User $user
     */
    public function updateUserInfo(UpdateRequest $request, User $user): void
    {

        $photo = $request->file('photo') ? $request->file('photo')->store('photos', 'public') : $user->photo;


       /* $user->update(
            array_merge($request->only
            ([
                'first_name',
                'last_name',
                'second_name',
                'email',
                'socials',
                'education',
                'skype',
                'phone'
            ]),
                ['photo' => $photo]
            )
        );*/
        // if there are was unchecked all notify with checkboxes , save empty array
        $update = array_merge($request->except(['photo']), ['photo' => $photo]);
        if( !$request->input('notify_with') ){
            $update['notify_with'] = [];
        }
        $user->update($update);

    }

    /**
     * @param Request $data
     * @param bool $isCandidate
     * @return User
     */
    public function createUser(Request $data, bool $isCandidate = false): User
    {
        $photo = $data->file('photo') ? $data->file('photo')->store('storage/photos', 'public') : null;

        $create = [
            "photo" => $photo,
        ] + $data->all();
        // update the notify with field with empty array if all
        // checkboxes are unchecked
        if( !$data->input('notify_with') ){
            $create['notify_with'] = [];
        }
        $user = new User($create);

        $user->status = $isCandidate ? User::STATUS_CANDIDATE : User::STATUS_EMPLOYEE;

        $user->save();

        $event = new Event();
        $event->event_id = 1;
        $event->title = 'Добавлен новый сотрудник';
        $event->hr_id = Auth::user()->id;
        $event->user_id = $user->id;
        $event->timestamp = date('Y-m-d H:i:s');
        $event->save();

        return $user;
    }
}