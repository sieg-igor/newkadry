<?php

namespace App\Services\Vacancy;

use App\Entities\Vacancy;
use App\Event;
use App\Http\Requests\Vacancy\CreateRequest;
use App\Http\Requests\Vacancy\UpdateRequest;
use Illuminate\Support\Facades\Auth;

/**
 * Class VacancyService
 */
class VacancyService
{
    /**
     * @param CreateRequest $data
     */
    public function createVacancy(CreateRequest $data): void
    {
        /*Vacancy::create([
            'title' => $data->title,
            'description' => $data->description,
            'salary' => $data->salary,
            'currency_id' => $data->currency,
            'city_id' => $data->city,
            'user_id' => Auth::id()
        ]);*/

        $vacancy = new Vacancy();
        $vacancy->title = $data->title;
        $vacancy->description = $data->description;
        $vacancy->salary = $data->salary;
        $vacancy->currency_id = $data->currency;
        $vacancy->city_id = $data->city;
        $vacancy->user_id = Auth::id();
        $vacancy->save();

        $event = new Event();
        $event->event_id = 6;
        $event->vacancy_id = $vacancy->id;
        $event->title = 'Добавлена новая вакансия';
        $event->timestamp = date('Y-m-d H:i:s');
        $event->save();
    }

    /**
     * @param UpdateRequest $data
     * @param Vacancy $vacancy
     */
    public function updateVacancy(UpdateRequest $data, Vacancy $vacancy): void
    {
        $vacancy->update([
            'title' => $data->title,
            'description' => $data->description,
            'salary' => $data->salary,
            'currency_id' => $data->currency,
            'city_id' => $data->city,
            'user_id' => Auth::id()
        ]);
    }

    /**
     * @param Vacancy $vacancy
     * @throws \Exception
     */
    public function deleteVacancy(Vacancy $vacancy): void
    {
        $vacancy->delete();
    }
}