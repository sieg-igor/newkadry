<?php
/**
 * Created by PhpStorm.
 * User: developer
 * Date: 28.11.18
 * Time: 10:22
 */

namespace App\Services;

use App\Entities\Data\RemoteAccess;
use GuzzleHttp\Client;
use App\Entities\User;
use Illuminate\Support\Facades\Log;

class RemoteAccessService
{

    public function getHeadersWithApiToken(RemoteAccess $site)
    {
        return [
            'Accept' => 'application/json',
            'Api-Token' => $site->api_token
        ];
    }

    public function requestCreateToUrl(RemoteAccess $site, User $user)
    {
        $client = new Client();
        if($site->api_token !== null)
        {
            $headers = $this->getHeadersWithApiToken($site);
        }
        $user = $user->toArray();
        try {
            $response = $client->request('POST', $site->url, ['headers' => $headers, 'form_params' => $user]);
        } catch (\Throwable $exception) {
            Log::alert([$exception->getCode(), $exception->getMessage()]);
            return false;
        }
        return $response;
    }

    public function requestDeleteToUrl(RemoteAccess $site, User $user)
    {
        $client = new Client();
        if($site->api_token !== null)
        {
            $headers = $this->getHeadersWithApiToken($site);
        }
        $user = $user->toArray();
        try {
            $response = $client->request('DELETE', $site->url, ['headers' => $headers, 'query' => $user]);
        } catch (\Throwable $exception) {
            Log::alert([$exception->getCode(), $exception->getMessage()]);
            return false;
        }
        return $response;
    }
}