<?php

namespace App\Services\Interview;

use App\Entities\Candidate;
use App\Entities\Data\Organizer;
use App\Entities\User;
use App\Event;
use App\Http\Requests\Candidate\CreateCandidateRecordRequest;
use App\Http\Requests\Candidate\CreateRequest;
use App\Http\Requests\Candidate\UpdateRequest;
use App\Http\Requests\Interview\UpdateRequest as InterviewUpdateRequest;
use App\Interview;
use App\InterviewEmployee;
use App\Notifications\InterviewNotification;
use App\Notifications\InterviewSlackNotification;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use App\Jobs\ProcessNotifications;
use Illuminate\Support\Facades\Notification;

/**
 * Class CandidateService
 */
class InterviewService
{
    /**
     * @param CreateRequest $data
     */

    public function createInterview($data)
    {
        //$creatorId = Auth::user()->id;

        DB::transaction(function () use ($data) {

        // alternative save
        $interview = new Interview();
        $interview->candidate_id = Input::get('candidate_id');
        $interview->position_id = Input::get('position_id');
        $interview->vacancy_id = Input::get('vacancy_id');
        $interview->city_id = Input::get('city_id');
        $interview->date_from = Input::get('date_from');
        $interview->date_to = Input::get('date_from');
        $interview->interviewer_id = 1;
        $interview->save();


        // add interviewers id to `interview_employees` table
        foreach (array_values($data->get('interviewer_id')) as $id) {
            $interviewEmployee = new InterviewEmployee();
            $interviewEmployee->interview_id = $interview->id;
            $interviewEmployee->user_id = $id;
            $interviewEmployee->save();
    //                $interview->interviewemployyes()->assosiate(User::findOrFail($id));
        }

        foreach ( array_values( $data->get('interviewer_id') ) as $id ){
            $user = User::findOrFail($id);
            $user->notify(new InterviewNotification($user, $interview));
        }

        // notification for the slack channel
        Notification::route('slack', config('services.slack.slack_webhook_url') )
            ->notify(new InterviewSlackNotification($interview));

        $description = "Создано новое собеседование на вакансию {$interview->position_id} которое состоится {$interview->date_from}";
//            Event::add(3, Event::NEW_INTERVIEW_ADDED, $description);

//            try{
//                Event::add(3, 'Создано новое собеседование', $interview->candidate_id, $interview->id,
//                    $interview->position_id, $interview->city_id, Auth::user()->id, '', date('Y-m-d H:i:s'));
//            } catch (\Exception $e){
//                Log::error($e->getMessage() . '   |   File:' . $e->getFile() . '   |   Line:' . $e->getLine());
//            }


        $event = new Event();
        $event->event_id = 3;
        $event->title = '';
        $event->user_id = $interview->candidate_id;
        $event->interview_id = $interview->id;
        $event->position_id = $interview->position_id;
        $event->city_id = $interview->city_id;
        $event->hr_id = Auth::user()->id;
        $event->description = '';
        $event->timestamp = date('Y-m-d H:i:s');
        $event->save();

        //add event to `organizer` table (for calendar)
        $organizer = new Organizer();
        $organizer->title = 'Новое интервью!';
        $organizer->start = Input::get('date_from');
        $organizer->color = '#FA5858';
        $organizer->end = Input::get('date_from');
        $organizer->interview_id = $interview->id;
        $organizer->save();

        });
    }

    public function updateInterview(InterviewUpdateRequest $data, Interview $interview): void
    {
        //сюда попадает! dd($dara)

//        $interview->update($data);

        //worked!
        $interview->update([
            'candidate_id' => $data->candidate_id,
            'position_id' => $data->position_id,
            'vacancy_id' => $data->vacancy_id,
            'city_id' => $data->city_id,
            'interviewer_id' => $data->interview_id, // null
            'date_from' => $data->date_from,
            'date_to' => $data->date_to,
        ]);

        //remove old interviewers data
//        $interviewEmployee = InterviewEmployee::where('interview_id', 86)->forceDelete();
//      $interviewEmployee->delete();

        //add new interviewers data
        /*foreach ($data->interviewer_id as $interviewer_id) {
            $interviewEmployees->interview_id = $data->interview_id;
            $interviewEmployees->user_id = $interviewer_id;
            $interviewEmployees->save();
        }*/
    }
}

/**
 * // alternative save
$interview = new Interview();
$interview->candidate_id = Input::get('candidate_id');
$interview->position_id = Input::get('position_id');
$interview->vacancy_id = Input::get('vacancy_id');
$interview->city_id = Input::get('city_id');
$interview->date_from = Input::get('date_from');
$interview->date_to = Input::get('date_to');

//collecting all interviewer-id's
$ids = '';
$i = 0;
foreach (array_values($data->get('interviewer_id')) as $id) {
$ids .= $id . ',';
$i++;
}
$ids = rtrim($ids, ','); // removing last comma "42,45," => "42,45"

$interview->interviewer_id = $ids;
$interview->save();

$ids = rtrim($ids, ','); // removing last comma "42,45," => "42,45"
 */