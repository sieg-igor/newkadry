<?php

namespace App;

use App\Entities\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class InterviewEmployee extends Model
{
    protected $fillable = ['interview_id', 'user_id'];

    public $timestamps = false;

    public function interview()
    {
        return $this->hasMany(Interview::class);
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
