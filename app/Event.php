<?php

namespace App;

use App\Entities\Candidate;
use App\Entities\Data\City;
use App\Entities\Data\EventType;
use App\Entities\Data\Position;
use App\Entities\Data\Vacancy;
use App\Entities\User;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Event extends Model
{
    //

    public $table = 'events';
    public $timestamps = false;

    protected $fillable = [];

    const UNDEFINED = 0;
    const NEW_USER_ADDED = 1;
    const NEW_POSITION_ADDED = 2;
    const NEW_VACATION_ADDED = 3;
    const NEW_CURRENCY_ADDED = 4;
    const NEW_CITY_ADDED = 5;
    const NEW_INTERVIEW_ADDED = 6;
    const NEW_INTERVIEWER_ADDED = 7;


    public function eventType(){
        return $this->hasOne(EventType::class, 'id', 'event_id');
    }

    public static function eventsList(): array
    {
        return [
            self::UNDEFINED => 'Что-то было сделано, но что - не понятно',
            self::NEW_USER_ADDED => 'Добавлен пользователь',
            self::NEW_POSITION_ADDED => 'Добавлена новая позиция',
            self::NEW_VACATION_ADDED => 'Добавлена новая вакансия',
            self::NEW_CURRENCY_ADDED => 'Добавлена новая валюта',
            self::NEW_CITY_ADDED => 'Добавлен новый город',
            self::NEW_INTERVIEW_ADDED => 'Добавлено новое интервью',
            self::NEW_INTERVIEWER_ADDED => 'Добавлен новый интервьювер',
        ];
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function candidate()
    {
        return $this->hasOne(Candidate::class, 'id', 'candidate_id');
    }

    public function interview()
    {
        return $this->hasOne(Interview::class, 'id', 'interview_id');
    }

    public function position()
    {
        return $this->hasOne(Position::class, 'id', 'position_id');
    }

    public function city(){
        return $this->hasOne(City::class, 'id', 'city_id');
    }

    public function currency(){
        return $this->hasOne(Currency::class, 'id', 'currency_id');
    }

    public function vacancy(){
        return $this->hasOne(Vacancy::class, 'id', 'vacancy_id');
    }

    public static function add($event_id, $title = null, $description = null, $user_id = null, $city_id = null, $interview_id = null, $hr_id = null)
    {
        $event = new Event();
        $event->event_id = $event_id;
        $event->title = $title;
        $event->description = $description;
        $event->user_id = $user_id;
        $event->interview_id = $interview_id;
        $event->hr_id = $hr_id;
        $event->timestamp = date('Y-m-d H:i:s');
        $event->save();
    }

    public static function getTitleById($id)
    {

    }

    public static function getAll()
    {

        $events = Event::with('eventType')
            ->with('user')
            ->with('interview')
            ->with('position')
            ->with('vacancy')
            ->with('city')
            ->with('candidate')
            ->orderBy('timestamp', 'desc')
            ->get();
        return $events;
    }

    public static function getJustEvents()
    {
        $events_array = Event::all();
        return $events_array;
    }

    public static function getBetween($start, $end)
    {
        $events_between = DB::table('events')
            ->whereDate('timestamp', '>', $start)
            ->whereDate('timestamp', '<', $end)
            ->get();

//        $events_between = Event::whereBetween('timestamp', [$start, $end])->get();

        return $events_between;
    }

    public static function getToday()
    {
        $events_today = Event::whereDate('timestamp', Carbon::today())->get();
        return $events_today;
    }

    public static function getYesterday()
    {
        $events_yesterday = Event::whereDate('timestamp', Carbon::yesterday())->get();
        return $events_yesterday;
    }

    public static function getWeek()
    {
        $now = Carbon::now();
        $events_week = Event::whereDate('timestamp', $now->startOfWeek())->get();
        return $events_week;
    }

    public static function getMonth()
    {
        $now = Carbon::now();
        $events_month = Event::whereDate('timestamp', $now->startOfMonth())->get();
        return $events_month;
    }

    public static function getUsersDate($day)
    {
        // DB::table()  - returns array[], $array['item']
        // Event::where - returns Object, $object->item

        $events_this_day = Event::where('timestamp', 'like', '%'.$day.'%')->get();
        return $events_this_day;
    }

    public static function getByType(int $type)
    {
        // DB::table()  - returns array[], $array['item']
        // Event::where - returns Object, $object->item

        $events_by_type = Event::where(['event_id' => $type])->get();
        return $events_by_type;
    }

    public static function getColor($type)
    {
        switch ($type){
            case 0:
                $class = 'secondary';
                break;
            case 1: // user
                $class = 'info';
                break;
            case 2:
                $class = 'warning';
                break;
            case 3: // interview
                $class = 'danger';
                break;
            case 6: // vacancy
                $class = '';
                break;
            case 7: //
                $class = 'warning';
                break;
            default:
                $class = 'default';
                break;
        }
        return $class;
    }

    //List of scopes
    public function scopeOfType($query, $type){

            switch($type) {
                case "today":
                    $date = Carbon::now('Europe/Zaporozhye');
                    $query->whereDate('timestamp', Carbon::today());
                    break;
                case "yesterday":
                    $date = Carbon::now('Europe/Zaporozhye');
                    $query->whereDate('timestamp', Carbon::yesterday());
                    break;
                case "week":
                    $date = Carbon::now('Europe/Zaporozhye');
                    $query->whereBetween('timestamp', [Carbon::now()->startOfWeek(),Carbon::now()->endOfWeek()]);
                    break;
                case "month":
                    $date = Carbon::now('Europe/Zaporozhye');
                    $query->whereBetween('timestamp', [Carbon::now()->startOfMonth(),Carbon::now()->endOfMonth()]);
                    break;
                case 'interview':
                    $query->where('event_id', 3);
                    break;
                case 'worker':
                    $query->where('event_id', 1);
                    break;
                case 'candidate':
                    $query->where('event_id', 2);
                    break;
                case 'vacancy':
                    $query->where('event_id', 6);
                    break;
                default:
                    throw new \Exception("Exception scope");
            }
        }

    public function scopeOfYear($query, $year){

    }

}