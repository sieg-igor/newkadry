<?php
namespace App\Http\Controllers\User;

use App\Entities\Candidate;
use App\Entities\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\CreateRequest;
use App\Http\Requests\User\UpdateRequest;
use App\Services\User\UserService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

/**
 * Class UserController
 */
class UserController extends Controller
{
    /**
     * @var UserService
     */
    private $service;

    /**
     * UserController constructor.
     * @param UserService $service
     */
    public function __construct(UserService $service)
    {
        $this->service = $service;
        $this->middleware('can:manage-users')->only(['changeStatus', 'destroy']);
    }

    /**
     * @return View
     */
    public function index(Request $request): View
    {
        if($request->query('fired') == 'true') {
            $users = User::firedStatus();
        }else {
            $users = User::workersStatus();
        }
        if($request->query('employer_search') != null) {
            $users->searchUser($request->query('employer_search'));
        }
        $users = $users->orderBy('last_name', 'asc')
            ->paginate(10);

        $statusesList = User::statusesList();

        return view('user.index',compact('users', 'statusesList'));
    }

    /**
     * @return View
     */
    public function create(): View
    {
        return view('user.create');
    }

    /**
     * @param CreateRequest $request
     * @return RedirectResponse
     */
    public function store(CreateRequest $request)
    {
        try {
            $this->service->createUser($request);
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        }
        return redirect()->route('user.index');
    }

    /**
     * @param User $user
     * @return View
     */
    public function edit(User $user): View
    {
        $this->checkAccess($user);
        return view('user.edit', compact('user'));
    }

    /**
     * @param UpdateRequest $request
     * @param User $user
     * @return RedirectResponse
     */
    public function update(UpdateRequest $request, User $user): RedirectResponse
    {
        $this->checkAccess($user);
        $this->service->updateUserInfo($request, $user);
        return redirect()->route('user.show', $user);
    }

    /**
     * @param User $user
     * @param int $status
     * @return RedirectResponse
     */
    public function changeStatus(User $user, int $status): RedirectResponse
    {
        try {
            $this->service->changeStatus($user, $status);
        } catch (\DomainException $e) {
            return back()->with('error', $e->getMessage());
        }
        return redirect()->route('user.show', $user->id);
    }

    /**
     * @param User $user
     * @return View
     */
    public function show(User $user): View
    {
        $statusesList = Candidate::statusesList();
        $userStatusesList = User::statusesList();
        return view('user.show', compact('user', 'statusesList', 'userStatusesList'));
    }

    /**
     * @param User $user
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy(User $user): RedirectResponse
    {
        $user->delete();
        return redirect()->back();
    }

    /**
     * @param User $user
     */
    public function checkAccess(User $user): void
    {
        if (!Gate::allows('edit-user-data', $user)) {
            abort(403);
        }
    }


}