<?php

namespace App\Http\Controllers\Candidate;

use App\Entities\Candidate;
use App\Entities\Data\DataDTO;
use App\Entities\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\Candidate\CreateCandidateRecordRequest;
use App\Http\Requests\Candidate\CreateRequest;
use App\Http\Requests\Candidate\UpdateRequest;
use App\Services\User\CandidateService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

/**
 * Class CandidateController
 */
class CandidateController extends Controller
{
    /**
     * @var CandidateService
     */
    private $service;

    /**
     * CandidateController constructor.
     * @param CandidateService $service
     */
    public function __construct(CandidateService $service)
    {
        $this->service = $service;
        $this->middleware('can:manage-users')->except(['index']);
    }

    /**
     * @param Request $request
     * @return View
     */
    public function index(Request $request)
    {
        $candidates = User::findUsersWithCandidateInfo($request->get('name'));

        $statusesList = Candidate::statusesList();

        return view('candidate.index',compact('candidates', 'statusesList'));
    }

    /**
     * @return View
     */
    public function create()
    {
        $data = new DataDTO();
        return view('candidate.create', compact('data'));
    }

    /**
     * @param CreateRequest $request
     * @return RedirectResponse
     */
    public function store(CreateRequest $request)
    {
        try {
            $this->service->createUser($request);
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        }
        return redirect()->route('candidate.index');
    }

    /**
     * @param Candidate $candidate
     * @param int $status
     * @return RedirectResponse
     */
    public function changeStatus(Candidate $candidate, int $status): RedirectResponse
    {
        try {
            $this->service->changeStatus($candidate, $status);
        } catch (\DomainException $e) {
            return back()->with('error', $e->getMessage());
        }
        return redirect()->route('user.show', $candidate->user);
    }

    public function edit(Candidate $candidate): View
    {
        $data = new DataDTO();
        return view('candidate.edit', compact('data', 'candidate'));
    }

    /**
     * @param UpdateRequest $request
     * @param User $candidate
     * @return RedirectResponse
     */
    public function update(UpdateRequest $request, User $candidate): RedirectResponse
    {
        try {
            $this->service->updateCandidate($request, $candidate);
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        }
        return redirect()->route('user.show', $candidate);
    }

    /**
     * @param User $user
     * @return View
     */
    public function createCandidateRecord(User $user): View
    {
        $data = new DataDTO();
        return view('candidate.create-candidate-record', compact('data', 'user'));
    }

    /**
     * @param CreateCandidateRecordRequest $request
     * @param User $user
     * @return RedirectResponse
     */
    public function storeCandidateRecord(CreateCandidateRecordRequest $request, User $user): RedirectResponse
    {
        try {
            $this->service->createCandidateRecord($request, $user);
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        }
        return redirect()->route('user.show', $user);
    }

    public function getList()
    {
        $data = new DataDTO();
        return $data->candidatesList;
    }

    public function getListMatch()
    {
        $match = Input::get('match');

        $candidates = User::with('candidate')
            ->where(function ($query) use ($match){
                $query->where('users.last_name', 'like', '%' . $match . '%')
                    ->orWhere('users.first_name', 'like', '%' . $match . '%')
                    ->orWhere('users.second_name', 'like', '%' . $match . '%')
                    ->orWhere('users.phone', 'like', '%' . $match . '%');
                })
            ->get();

//        $candidates = Candidate::with('user')
//            ->select('candidates.id', 'users.id')
//            ->where(function ($query) use ($match){
//                $query->where('users.last_name', 'like', '%' . $match . '%')
//                    ->orWhere('users.first_name', 'like', '%' . $match . '%')
//                    ->orWhere('users.second_name', 'like', '%' . $match . '%')
//                    ->orWhere('users.phone', 'like', '%' . $match . '%');
//            })->get();

//        $candidates = User::search($match)->get();

        return $candidates;

    }

    public function getCandidatesForInterview()
    {
        $candidates = User::with('candidate')
            ->where('users.status', User::STATUS_CANDIDATE)
            ->get();
        $candidates_for_interview = $candidates->reject(function ($value, $key) {
            return $value->candidate->status !== Candidate::STATUS_WAITING_INTERVIEW;
        });
        return response($candidates_for_interview);
    }

    public function stat()
    {
        $candidates = User::findUsersWithCandidateInfoAnyStatus()->toArray();
        $candidates = $candidates['data'];
        $statusesList = Candidate::statusesList();
        return view('candidate.stat',compact('candidates', 'statusesList'));
    }

}