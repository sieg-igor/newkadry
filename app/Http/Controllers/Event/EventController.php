<?php

namespace App\Http\Controllers\Event;

use App\Entities\Data\Organizer;
use App\Event;
use App\Http\Requests\Events\StoreEventsRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use MaddHatter\LaravelFullcalendar\Facades\Calendar;

class EventController extends Controller
{
    /* This is a Calendar. Not `events` on main page */

    public function index()
    {

        $events = [];

//        working version
        Organizer::where('id', '>', 0)
            ->each(function ($item) use (&$events) {
                $events[] = Calendar::event($item->title, false, $item->start, $item->end, $item->id, ['color' => $item->color]);
            });


        $calendar = Calendar::addEvents($events)
            ->setOptions([
                'locale' => 'ru',
                'timeFormat' => 'H:mm',
                'buttonText' => ['today' => 'сегодня', 'month'=>'месяц','week'=>'неделя','day'=>'день','list'=>'список'],
                'views' => [
                    'week' => [
                        'columnHeaderFormat'=> 'D.M ddd',
                        'slotLabelFormat'=> 'HH:mm',
                    ],
                    'day' => [
                        'slotLabelFormat'=> 'HH:mm',
                    ]
                ],
                'eventLimit' => true,
                'firstDay' => 1
            ]);

        $calendar->script();

        return view('calendar.index',
            [
                'calendar' => $calendar,
            ]);
    }


    public function create(StoreEventsRequest $request)
    {
        Organizer::create($request->all());

        return redirect()
            ->route('calendar.add')
            ->with('message', 'Событие успешно добавлено!');
    }

    public function add()
    {
        return view('calendar.add');
    }

}
