<?php

namespace App\Http\Controllers\Data;

use App\Http\Requests\RemoteAccessRequest;
use Illuminate\Http\Request;
use App\Entities\Data\RemoteAccess;

class RemoteAccessController
{
    public function index()
    {
        $accesses = RemoteAccess::paginate(10);
        return view('data.remote-access.index', compact('accesses'));
    }

    public function create()
    {
        return view('data.remote-access.create');
    }

    public function store(RemoteAccessRequest $request)
    {
        RemoteAccess::create($request->all());

        return redirect()->route('data.remote-access.index');
    }

    public function edit(RemoteAccess $remote_access)
    {
        return view('data.remote-access.edit', compact('remote_access'));
    }

    public function update(RemoteAccessRequest $request, RemoteAccess $remote_access)
    {
        $remote_access->update($request->all());

        return redirect()->route('data.remote-access.index');
    }

    public function destroy(RemoteAccess $remote_access)
    {
        $remote_access->delete();

        return redirect()->route('data.remote-access.index');
    }
}
