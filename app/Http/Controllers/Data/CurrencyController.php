<?php
namespace App\Http\Controllers\Data;

use App\Entities\Data\Currency;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Validation\Rule;

/**
 * Class CurrencyController
 */
class CurrencyController
{
    /**
     * @return View
     */
    public function index()
    {
        $currencies = Currency::orderBy('title')->paginate(10);
        return view('data.currency.index', compact('currencies'));
    }

    /**
     * @return View
     */
    public function create()
    {
        return view('data.currency.create');
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|string|max:255|unique:currencies',
        ]);

        Currency::create(['title' => $request['title']]);

        return redirect()->route('data.currency.index');
    }

    /**
     * @param Currency $currency
     * @return View
     */
    public function edit(Currency $currency)
    {
        return view('data.currency.edit', compact('currency'));
    }

    /**
     * @param Request $request
     * @param Currency $currency
     * @return RedirectResponse
     */
    public function update(Request $request, Currency $currency)
    {
        $request->validate([
            'title' => [
                'required', 'string', 'max:255', Rule::unique('currencies')->ignore($currency->id),
            ]]);
        $currency->update(['title' => $request['title']]);

        return redirect()->route('data.currency.index');
    }

    /**
     * @param Currency $currency
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy(Currency $currency)
    {
        $currency->delete();
        return redirect()->route('data.currency.index');
    }
}