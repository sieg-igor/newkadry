<?php

namespace App\Http\Controllers;

use App\Entities\Candidate;
use App\Entities\Data\City;
use App\Entities\Data\EventType;
use App\Entities\Data\Position;
use App\Entities\Data\User;
use App\Entities\Data\Vacancy;
use App\Event;
use App\Interview;
use http\Url;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Input;

class HomeController extends Controller
{
    // `events` are here
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()

    {
        if(Input::has("type")) {
            $type = Input::get('type');
        } else {
            $type = Cookie::get('type');
        }

        $query = Event::orderBy('timestamp', 'desc');
        if(!empty($type)) {
            $query->ofType($type);
        }
//        $events = $query->get();
        $events = $query->paginate(10);
//        dd($events);

        setcookie('type', $type, 60*60*24);

        $cityIds = $events->pluck("city_id")->unique();
        $cities = City::whereIn('id', $cityIds)->pluck("title", "id");

        $positionIds = $events->pluck("position_id")->unique()->filter();
        $positions = Position::whereIn('id', $positionIds)->pluck("title", "id");

        $candidatesIds = $events->pluck("candidate_id")->unique();
        $candidates = Candidate::whereIn('id', $candidatesIds)->pluck("user_id", "id");

        $vacanciesIds = $events->pluck("vacancy_id")->unique()->filter();
        $vacancies = Vacancy::whereIn('id', $vacanciesIds)->pluck("title", "id");

        $eventTypeIds = $events->pluck("event_id")->unique()->filter();
        $event_types = EventType::whereIn('id', $eventTypeIds)->pluck('title', 'id');

        $interviewIds = $events->pluck("interview_id")->unique()->filter();
        $interviews = Interview::whereIn('id', $interviewIds)->get(['id', 'date_from', 'date_to'])->keyBy("id")->toArray();

        $usersIds = $events->pluck("user_id")->unique()->filter();

        $users = User::whereIn('id', $usersIds)->get(['id', 'first_name', 'last_name','second_name'])->keyBy("id");


        return view('home', compact('events', 'btn_class', 'type',
            'cities', 'positions', 'candidates', 'vacancies', 'event_types', 'interviews', 'users'));
    }
}
