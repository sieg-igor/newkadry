<?php
namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdateRequest
 */
class UpdateRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'first_name' => 'required|string:40',
            'last_name' => 'required|string:40',
            'second_name' => 'nullable|string:40',
            'email' => 'required|string|email|max:255|unique:users,id,' . $this->user->id,
            'socials' => 'nullable|string',
            'education' => 'nullable|string',
            'photo' => 'nullable|file|mimes:png,jpg,jpeg',
            'phone' => 'nullable|string',
            'skype' => 'nullable|string',
            'employer_at' => 'nullable|date',
            'fired_at' => 'nullable|date'
        ];
    }
}