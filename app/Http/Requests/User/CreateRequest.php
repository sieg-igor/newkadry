<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;


/**
 * Class CreateRequest
 */
class CreateRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    public static function getRules()
    {
        return [
            'first_name' => 'required|string:40',
            'last_name' => 'required|string:40',
            'second_name' => 'nullable|string:40',
            'email' => 'required|string|email|max:40|unique:users',
            'photo' => 'nullable|file|mimes:png,jpg,jpeg',
            'socials' => 'nullable|string',
            'education' => 'nullable|string',
            'phone' => 'nullable|string',
            'skype' => 'nullable|string',
        ];
    }


    /**
     * @return array
     */
    public function rules(): array
    {
        return self::getRules();
    }


}