<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RemoteAccessRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
                {
                    return [];
                }
            case 'POST':
                {
                    return [
                        'url' => [
                            'required',
                            'url',
                            Rule::unique('remote_accesses'),
                        ],
                        'api_token' => 'required|string',
                    ];
                }
            case 'PUT':
            case 'PATCH':
                {
                    return [
                        'url' => [
                            'required',
                            'url',
                            Rule::unique('remote_accesses')->ignore($this->remote_access->id),
                            ],
                        'api_token' => 'required|string',
                    ];
                }
            default:break;
        }
    }
}
