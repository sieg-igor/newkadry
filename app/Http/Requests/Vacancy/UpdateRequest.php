<?php

namespace App\Http\Requests\Vacancy;

use App\Entities\Data\DataDTO;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class UpdateRequest
 */
class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $data = new DataDTO();
        return [
            'title' => 'required|string:50|unique:vacancies,id,' . $this->vacancy->id,
            'description' => 'required|string',
            'salary' => 'nullable|integer',
            'currency' => ['nullable', 'integer', Rule::in(array_keys($data->currenciesList))],
            'city' => ['required', 'integer', Rule::in(array_keys($data->citiesList))],
        ];
    }
}
