<?php

namespace App\Jobs;

use App\Entities\User;
use App\Interview;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use meatmanFS\Notificator\Facades\Notifier;

class ProcessNotifications implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;
    protected $interview;

    public $tries = 1;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct( Interview $interview, User $user )
    {
        $this->interview = $interview;
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user = $this->user;
        $interview = $this->interview;
        Notifier::to($user->email)//email
        ->subject('Напоминание о собеседование')// subject
        ->data( compact('interview', 'user') )// message
        ->with([ // the array with what is send notification
            [// can be plain 'mail' (will use default values)
                'mail' => [// here is the data to pass in the template and the custom template name
                    'template' => 'mail.notify',
                ]
            ],
            'telegram',
            'skype',
            [
                'slack' => [
                    'default_channel' => '#newkadry',
                ]
            ],
        ])->send();// send
    }
}
