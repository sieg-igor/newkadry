<?php

namespace App\Jobs;

use App\Entities\User;
use App\Event;
use App\Entities\Data\RemoteAccess;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Application;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
Use App\Jobs\ProcessChangeUserStatusStack;

class ProcessChangeUserStatus //implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;
    protected $param;

    public $tries = 1;

    /**
     * Create a new job instance.
     * @param string $param
     * @return void
     */
    public function __construct(User $user, int $param)
    {
        $this->user = $user;
        $this->param = $param;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $sites = RemoteAccess::all();
        if ($sites)
        {
            foreach ($sites as $site) {
                ProcessChangeUserStatusStack::dispatch($site, $this->user, $this->param);
            }
        }
    }
}
