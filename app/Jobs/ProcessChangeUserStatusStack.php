<?php

namespace App\Jobs;

use App\Services\RemoteAccessService;
use App\Entities\Data\RemoteAccess;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Exception;
use App\Entities\User;

class ProcessChangeUserStatusStack implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $site;
    protected $user;
    protected $param;

    public $tries = 0;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(RemoteAccess $site, User $user, int $param)
    {
        $this->site = $site;
        $this->user = $user;
        $this->param = $param;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(RemoteAccessService $remoteAccessService)
    {
        switch ($this->param)
        {
            case User::STATUS_EMPLOYEE:
                $response = $remoteAccessService->requestCreateToUrl($this->site, $this->user);
                $this->checkResponse($response);
                break;
            case User::STATUS_FIRED:
                $response = $remoteAccessService->requestDeleteToUrl($this->site, $this->user);
                $this->checkResponse($response);
                break;
        }
    }

    public function checkResponse($response)
    {
        if ($response === false || $response->getStatusCode() !== 200) {
            throw new Exception("Job can't be done: {$this->site->url}, {$this->param}, {$this->user->email}\n
                    watch detailed information in logs");
        }
    }
}
