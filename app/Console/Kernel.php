<?php

namespace App\Console;

use App\Interview;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Carbon;
use App\Entities\User;
use Illuminate\Support\Facades\Cache;
use App\Jobs\ProcessNotifications;
use Illuminate\Support\Facades\Notification;
use App\Notifications\InterviewNotification;
use App\Notifications\InterviewSlackNotification;


class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {

            $send_ids = Cache::get('Notifier_Interview_Send_Ids', array());

            $interviews = Interview::whereDate('date_from', Carbon::now()->format('Y-m-d'))
                ->whereNotIn('id', $send_ids)
                ->whereTime('date_to', '<=', Carbon::now()->addHours(2)->format('H:i'))
                ->whereTime('date_to', '>', Carbon::now()->format('H:i'))
                ->with(['interviewEmployee', 'user'])->get();


            $sending_ids = $interviews->pluck('id')->toArray();
            if( !empty( $sending_ids ) ){
                $send_ids = array_merge( $send_ids, $interviews->pluck('id')->toArray() );
                Cache::put('Notifier_Interview_Send_Ids', $send_ids, Carbon::now()->addHours(2) );
            }

            foreach ( $interviews as $interview ){
                foreach($interview->interviewEmployee as $interviewEmployee){
                    $user = User::where('id', $interviewEmployee->user_id)->first();
                    $user->notify(new InterviewNotification($user, $interview));
                }

                // notification for the slack channel
                Notification::route('slack', config('services.slack.slack_webhook_url') )
                    ->notify(new InterviewSlackNotification($interview));
            }

        })->everyThirtyMinutes();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
