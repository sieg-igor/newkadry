<?php

namespace App;

use App\Entities\Candidate;
use App\Entities\Data\City;
use App\Entities\Data\Organizer;
use App\Entities\Data\Position;
use App\Entities\Data\Vacancy;
use App\Entities\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Interview extends Model
{
    protected $guarded = ['id'];

    protected $fillable = [
        'candidate_id',
        'position_id',
        'vacancy_id',
        'city_id',
        'interviewer_id',
        'date_from',
        'date_to',
    ];

    public function rules()
    {

    }

    public function candidate(): BelongsTo
    {
        return $this->belongsTo(Candidate::class, 'candidate_id', 'user_id');
    }

    public function city(): BelongsTo
    {
        return $this->belongsTo(City::class, 'city_id', 'id');
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'candidate_id', 'id');
    }

    public function position(): BelongsTo
    {
        return $this->belongsTo(Position::class, 'position_id', 'id');
    }

    public function event(){
        return $this->hasOne(Event::class, 'id', 'interview_id');
    }


    public function interviewEmployee()
    {
        return $this->hasMany(InterviewEmployee::class);
    }

    public function organizer()
    {
        return $this->hasMany(Organizer::class, 'created_at', 'created_at');
    }

    public function vacancy()
    {
        return $this->hasMany(Vacancy::class, 'id', 'vacancy_id');
    }

    public static function getAll()
    {

        $interview = Interview::with('candidate')
            ->with('city')
            ->with('user')
            ->with('position')
            ->with('interviewEmployee')
            ->where('date_from', '>', date('Y-m-d', time()-86400)) // just fresh interviews
            ->orderBy('date_from', 'asc')
//            ->limit(1)
            ->paginate(4);
            /*->get();*/

        return $interview;
    }

/* BEGIN just for local test */

    public static function gelAllInterviewInfo()
    {
        $all_interview_info = Interview::with('interviewEmployee')
            ->get();
        return $all_interview_info;
    }
/* END just for local test */

    public static function getMonthOnNumber($number)
    {
        switch ($number) {
            case 01:
                $month = 'Январь';
                break;
            case 02:
                $month = 'Февраль';
                break;
            case 03:
                $month = 'Март';
                break;
            case 04:
                $month = 'Апрель';
                break;
            case 05:
                $month = 'Май';
                break;
            case 06:
                $month = 'Июнь';
                break;
            case 07:
                $month = 'Июль';
                break;
            case '08':
                $month = 'Август';
                break;
            case '09':
                $month = 'Сентябрь';
                break;
            case 10:
                $month = 'Октябрь';
                break;
            case 11:
                $month = 'Ноябрь';
                break;
            case 12:
                $month = 'Декабрь';
                break;
        }

        return $month;
    }

}
