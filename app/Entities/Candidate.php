<?php

namespace App\Entities;

use App\Entities\Data\City;
use App\Entities\Data\Company;
use App\Entities\Data\Currency;
use App\Entities\Data\Position;
use App\Interview;
use function foo\func;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\DB;
use Nicolaslopezj\Searchable\SearchableTrait;

/**
 * Class Candidate
 */
class Candidate extends Model
{
    use SearchableTrait;

    protected $searchable = [
        'columns' => [
            'candidates.user_id' => 20,
            'candidates.position_id' => 19,
            'candidates.city_id' => 18,
            'candidates.status' => 10,
        ]

    ];

    const STATUS_GOT_OFFER = -2;    // получил другое предложение
    const STATUS_FIRED = -1;        // уволен
    const STATUS_DIALOG_OPEN = 1;   // открыт диалог
    const STATUS_WAITING_INTERVIEW = 2; // назначено интервью
    const STATUS_ACCEPTED = 3; // принят
    const STATUS_REJECTED = 4; // отказали
    const STATUS_REFUSED = 5; // сам отказался


    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * @return BelongsTo
     */
    public function recruiter(): BelongsTo
    {
        return $this->belongsTo(User::class, 'recruiter_id', 'id');
    }

    /**
     * @return BelongsTo
     */
    public function position(): BelongsTo
    {
        return $this->belongsTo(Position::class, 'position_id', 'id');
    }

    /**
     * @return BelongsTo
     */
    public function currency(): BelongsTo
    {
        return $this->belongsTo(Currency::class, 'currency_id', 'id');
    }

    /**
     * @return BelongsTo
     */
    public function city(): BelongsTo
    {
        return $this->belongsTo(City::class, 'city_id', 'id');
    }

    /**
     * @return BelongsTo
     */
    public function interview(): BelongsTo
    {
        return $this->belongsTo(Interview::class, 'user_id', 'candidate_id');
    }

    public function event()
    {
        return $this->hasOne(Event::class, 'event_id', 'id');
    }

    /**
     * @return BelongsTo
     */
    public function lastCompany(): BelongsTo
    {
        return $this->belongsTo(Company::class, 'last_company_id', 'id');
    }

    /**
     * @return array
     */
    public static function statusesList(): array
    {
        return [
            self::STATUS_GOT_OFFER => 'Дали оффер',
            self::STATUS_FIRED => 'Уволен',
            self::STATUS_DIALOG_OPEN => 'Открыт диалог',
            self::STATUS_WAITING_INTERVIEW => 'Назначено собеседование',
            self::STATUS_ACCEPTED => 'Принят',
            self::STATUS_REJECTED => 'Не принят',
            self::STATUS_REFUSED => 'Отказался'
        ];
    }

    public static function getBadgeColor($status)
    {
        switch ($status){
            case 'Дали оффер':
                return 'default';
                break;
            case 'Уволен':
                return 'danger';
                break;
            case 'Открыт диалог':
                return 'success';
                break;
            case 'Назначено собеседование':
                return 'warning';
                break;
            case 'Принят':
                return 'success';
                break;
            case 'Не принят':
                return 'danger';
                break;
            case 'Отказался':
                return 'default';
                break;
            default:
                return 'primary';
                break;
        }
    }

    /**
     * @return bool
     */
    public function isAccepted(): bool
    {
        return $this->status === self::STATUS_ACCEPTED;
    }


    /*public static function getAll(){

        $candidates = Candidate::all();

        return $candidates->load('user')
                          ->load('position')
                          ->load('city');
    }*/

    public static function getAll(){

        $candidates = Candidate::with('user')
            ->with('position')
            ->with('city')
            ->with('interview')

            ->get();

        return $candidates;
    }

    public static function searchCandidates(){

        $candidates = Candidate::search($query)
            ->with('user')
            ->get();

        return $candidates;
    }

    public function scopeWithUserInfo($query)
    {
        return $query->with('user');
    }

    public function scopeWithUserInfoMatch($query, $pattern)
    {
        return $query->with('user')
            ->where(function ($query) use ($pattern){
                $query->where('last_name', '=', $pattern)
                    ->orWhere('first_name', '=', $pattern);
            });
    }

}
