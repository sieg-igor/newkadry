<?php

namespace App\Entities;

use App\InterviewEmployee;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use meatmanFS\Notificator\Facades\Notifier;
use Nicolaslopezj\Searchable\SearchableTrait;
use App\Jobs\ProcessChangeUserStatus;
use App\Services\User\UserService;

/**
 * Class User
 * @property Candidate $candidate
 */
class User extends Authenticatable
{
    use Notifiable;
    use SearchableTrait;

    protected $searchable = [
        'columns' => [
             'users.first_name' => 20,
             'users.last_name' => 19,
             'users.second_name' => 18,
             'users.phone' => 17,
        ],
    ];

    const STATUS_GOT_OFFER = -2;
    const STATUS_FIRED = -1;
    const STATUS_HR = 1;
    const STATUS_INTERVIEWER = 2;
    const STATUS_CANDIDATE = 3;
    const STATUS_EMPLOYEE = 4;
    const STATUS_TEAM_LEAD = 5;
    const STATUS_DIRECTOR = 100;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'second_name',
        'email',
        'password',
        'photo',
        'socials',
        'education',
        'status',
        'phone',
        'skype',
        'telegram',
        'slack',
        'notify_with',
        'employer_at',
        'fired_at'
    ];

    protected $casts = [
        'notify_with' => 'array',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @return bool
     */
    public function isCandidate(): bool
    {
        return $this->status === self::STATUS_CANDIDATE;
    }

    /**
     * @return bool
     */
    public function isHR(): bool
    {
        return $this->status === self::STATUS_HR;
    }

    /**
     * @return bool
     */
    public function setEmployeeStatus(): bool
    {
        if ($this->status !== self::STATUS_EMPLOYEE) {
            $userService = new UserService;
            $userService->changeStatus($this, self::STATUS_EMPLOYEE);
            return true;
        }
        return false;
    }

    /**
     * @return bool
     */
    public function setCandidateStatus(): bool
    {
        return $this->update(['status' => self::STATUS_CANDIDATE]);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function candidatesHistory()
    {
        return $this->hasMany(Candidate::class, 'user_id', 'id')->orderBy('updated_at', 'desc');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function candidate()
    {
        return $this->hasOne(Candidate::class, 'user_id', 'id')->orderBy('candidates.updated_at', 'desc');
    }

    public function event()
    {
        return $this->hasOne(Event::class, 'user_id', 'id');
    }

    /**
     * @param Builder $builder
     * @return Builder
     */
    public function scopeCandidatesStatus(Builder $builder)
    {
        return $builder->where('users.status', self::STATUS_CANDIDATE);
    }

    /**
     * @param Builder $builder
     * @return Builder
     */
    public function scopeWorkersStatus(Builder $builder)
    {
        return $builder->where('users.status', '!=', self::STATUS_CANDIDATE)
            ->where('users.status', '!=', self::STATUS_FIRED);
    }

    /**
     * @param Builder $builder
     * @return Builder
     */
    public function scopeFiredStatus(Builder $builder)
    {
        return $builder->where('users.status', self::STATUS_FIRED);
    }

    public static function scopeSearchUser(Builder $builder, $search_string)
    {
        return $builder->select('id', 'first_name', 'last_name', 'second_name', 'email', 'status', 'photo', 'fired_at', 'employer_at')
            ->where('users.last_name', 'like', $search_string. '%')
            ->orWhere('users.first_name', 'like', $search_string. '%')
            ->orWhere('users.second_name', 'like', $search_string. '%');
    }
    /**
     * @return array
     */
    public static function statusesList(): array
    {
        return [
            self::STATUS_GOT_OFFER => 'Дали оффер',
            self::STATUS_FIRED => 'Уволен',
            self::STATUS_HR => 'HR',
            self::STATUS_INTERVIEWER => 'Проводящий интервью',
            self::STATUS_CANDIDATE => 'Кандидат',
            self::STATUS_EMPLOYEE => 'Работник',
            self::STATUS_TEAM_LEAD => 'Team-lead',
            self::STATUS_DIRECTOR => 'Директор'
        ];
    }

    public function interviewEmployee()
    {
        return $this->hasOne(InterviewEmployee::class, 'user_id', 'id');
    }

    /**
     * @param null|string $name
     * @return mixed
     */
    public static function findUsersWithCandidateInfo($name = false)
    {
        $query = User::candidatesStatus()
            ->with(['candidate' => function ($query) { // подгружаем связь candidate() -> hasOne
                $query->with('recruiter', 'position', 'city', 'currency'); // у которого берём перечисленные позиции
            }]);
        if ($name) {
            $query->where(function ($query) use ($name) {
                $query->where('first_name', $name)
                    ->orWhere('second_name', $name)
                    ->orWhere('last_name', $name);
            });
        }
        return $query->orderBy('users.created_at', 'desc')
            ->paginate(10);
    }

    /**
     * @return bool
     */
    public function updateUpdatedAt()
    {
        return $this->update(['updated_at' => Carbon::now()]);
    }

    public static function getAllUsers()
    {
        //return self::pluck('id', 'first_name', 'last_name', 'id')->toArray();
        $users = User::all('id', 'first_name', 'last_name');
        return $users;
    }

    public static function getAllHRUsers()
    {
        $users = DB::table('users')
            ->select('id', 'first_name', 'last_name')
            ->where('status', '=', 1)   //    + HR
            ->orWhere('status', '=', 5) // + TEAM_LEADS
            ->orWhere('status', '=', 100) // + DIRECTOR
            ->get();

        return $users;
    }

    public static function _findUsersWithCandidateInfo()
    {
        $query = User::candidatesStatus()
            ->with(['candidate' => function ($query) { // подгружаем связь candidate() -> hasOne
                $query->with('recruiter', 'position', 'city', 'currency'); // у которого берём перечисленные позиции
            }]);

        return $query;
    }

    public static function getUserById($id){
        $users = DB::table('users')
            ->where('id', '=', $id)
            ->get();

        return $users[0]->first_name;
    }

    public function routeNotificationForSlack()
    {
        return $this->slack;
    }

    public function routeNotificationForTelegram()
    {
        if( !is_numeric( $this->telegram ) ){ // if not telegram chat id
            // get chat id from the telegram api
            $telegram_chat_id = Notifier::get_telegram_chat_id($this->telegram);
            if( !empty( $telegram_chat_id ) ){
                $this->telegram = $telegram_chat_id;
                $this->save();// save chat id to the user telegram column
            }
        }
        return $this->telegram;
    }

    public function scopeWithAnyStatus(Builder $builder)
    {
        return $builder->where(function ($query) {
            $query->where('users.status', '=', self::STATUS_EMPLOYEE)
                ->orWhere('users.status', '=', self::STATUS_CANDIDATE)
                ->orWhere('users.status', '=', self::STATUS_FIRED)
                ->orWhere('users.status', '=', self::STATUS_GOT_OFFER);
        });
    }

    public static function findUsersWithCandidateInfoAnyStatus($name = false)
    {
        $query = User::WithAnyStatus()
            ->with(['candidate' => function ($query) { // подгружаем связь candidate() -> hasOne
                $query->with('recruiter', 'position', 'city', 'currency'); // у которого берём перечисленные позиции
            }]);
        if ($name) {
            $query->where(function ($query) use ($name) {
                $query->where('first_name', $name)
                    ->orWhere('second_name', $name)
                    ->orWhere('last_name', $name);
            });
        }
        return $query->orderBy('users.updated_at', 'desc')
            ->paginate(10);
    }

}
