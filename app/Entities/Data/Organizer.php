<?php

namespace App\Entities\Data;

use App\Interview;
use Illuminate\Database\Eloquent\Model;

class Organizer extends Model implements \MaddHatter\LaravelFullcalendar\Event
{
    //

    protected $table = 'organizer';

    protected $fillable = [
        'title',
        'color',
        'interview_id',
        'description',
        'start',
        'end',
    ];

    public function rules(){
        return [
        'start' => 'string',
        'title' => 'required|min:6',
        'body' => 'required|min:6',
        ];
    }


    /**
     * Get the event's id number
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the event's title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Is it an all day event?
     *
     * @return bool
     */
    public function isAllDay()
    {
        return (bool)$this->all_day;
    }

    /**
     * Get the start time
     *
     * @return DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Get the end time
     *
     * @return DateTime
     */
    public function getEnd()
    {
        return $this->end;
    }

    public function interview()
    {
        return $this->hasMany(Interview::class, 'created_at', 'created_at');
    }


}
