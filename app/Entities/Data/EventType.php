<?php

namespace App\Entities\Data;

use App\Event;
use Illuminate\Database\Eloquent\Model;

class EventType extends Model
{
    //

    protected $table = 'event_type';

    public function event()
    {
        return $this->hasOne(Event::class, 'event_id', 'id');
    }

}
