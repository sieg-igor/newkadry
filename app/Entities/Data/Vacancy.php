<?php

namespace App\Entities\Data;

use Illuminate\Database\Eloquent\Model;

/**
 * Class City
 */
class Vacancy extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['title, description, salary'];

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return array
     */
    public static function getAllVacancies(): array
    {
        return self::pluck('title', 'id')->toArray();
    }
}
