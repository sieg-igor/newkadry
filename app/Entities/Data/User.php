<?php

namespace App\Entities\Data;

use Illuminate\Database\Eloquent\Model;

/**
 * Class City
 */
class User extends Model
{

    /**
     * @var array
     */
    protected $fillable = ['title'];

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return array
     */
    public static function getAllUsers(): array
    {
        return self::pluck('first_name', 'last_name', 'id')->toArray();
    }
}
