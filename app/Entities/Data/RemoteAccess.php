<?php

namespace App\Entities\Data;

use Illuminate\Database\Eloquent\Model;

class RemoteAccess extends Model
{
    protected $table = 'remote_accesses';
    protected $fillable = ['url', 'api_token'];

}
