<?php

namespace App\Entities\Data;

use App\Event;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Currency
 */
class Currency extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['title'];

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return array
     */
    public static function getAllCurrencies(): array
    {
        return self::pluck('title', 'id')->toArray();
    }

    public function event(){
        return $this->hasOne(Event::class, 'currency_id', 'id');
    }
}
