<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;
use meatmanFS\Notificator\Lib\SkypeMessage;
use meatmanFS\Notificator\Lib\SkypeChannel;
use Illuminate\Notifications\Messages\SlackMessage;
use Illuminate\Support\Facades\View;
use App\Entities\User;

class InterviewNotification extends Notification implements ShouldQueue
{
    use Queueable;

    public $tries = 3;

    protected $user;
    protected $interview;
    protected $message;
    protected $template = 'notifier.message';

    private $user_channels = [
        'email'     => 'mail',
        'slack'     => 'slack',
        'telegram'  => TelegramChannel::class,
        'skype'     => SkypeChannel::class,
    ];

    public function __construct( User $user, $interview )
    {
        $this->user = $user;
        $this->interview = $interview;
    }

    public function maxTries()
    {
        return 5;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        if( !empty( $this->user ) ){
            $user_channels = [];
            foreach ( (array)$this->user->notify_with as $channel ){
                if( array_key_exists( $channel, (array)$this->user_channels ) ){
                    $user_channels[] = $this->user_channels[$channel];
                }
            }
            return $user_channels;
        }
        return [
            'mail',
            'slack',
            TelegramChannel::class,
            SkypeChannel::class,
        ];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Напоминание о собеседование')
            ->markdown('mail.notify', ['interview'=>$this->interview ]);
    }

    public function toSlack()
    {
        return $this->get_slack_message();
    }

    public function toTelegram($notifiable)
    {
        return TelegramMessage::create()
            ->content($this->message());
    }

    public function toSkype( $notifiable )
    {

        return SkypeMessage::create()
            ->to($this->user->skype)
            ->message(  $this->message() ) ;
    }

    protected function get_slack_message()
    {
        $this->message();
        $slack_message = (new SlackMessage())->content( $this->message());

        return $slack_message;
    }

    protected function message()
    {
        if( empty( $this->message )){
            $view = View::make( $this->template, ['interview'=>$this->interview ] );
            $this->message = $view->render();
        }
        return $this->message;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
