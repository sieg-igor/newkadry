<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\SlackMessage;
use Illuminate\Support\Facades\View;

class InterviewSlackNotification extends Notification implements ShouldQueue
{
    use Queueable;

    public $tries = 1;

    protected $interview;
    protected $message;
    protected $template = 'notifier.message';

    public function __construct( $interview )
    {
        $this->interview = $interview;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['slack'];
    }

    public function toSlack()
    {
        return $this->get_slack_message();
    }

    protected function get_slack_message()
    {
        $this->message();
        $slack_message = (new SlackMessage())->content( $this->message());

        return $slack_message;
    }

    protected function message()
    {
        if( empty( $this->message )){
            $view = View::make( $this->template, ['interview'=>$this->interview ] );
            $this->message = $view->render();
        }
        return $this->message;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
