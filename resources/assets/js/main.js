$(document).ready(function () {

    // redirect to date chosen in calendar
    $('.fc-day, .fc-day, .fc-widget-content, .fc-thu, .fc-future').click(function () {
        alert(12);
    });

    // choosing an interval with dropdown select on homepage
    $('select#homepageSelectEventInterval').on('change', function () {
        let value = this.value;
        window.location.href = value;
    });

    $('select#homepageSelectEventType').on('change', function () {
        let value = this.value;
        window.location.href = value;
    });

    /*$('.fc-day, .fc-day-top').click(function () {
        var date = $(this).data('date');
        window.location.href = '/?interval=' +date;
    });*/


    /**
     * Crayola colors in JSON format
     * from: https://gist.github.com/jjdelc/1868136
     */


    function autocompleteCandidates(){

    }


$('input#nope').on("input", function () {

    let candidates = [];
    $.ajax({
        url: '/interview/candidates/all',
        method: 'GET',
        success: function (response) {
            // $.each(response, function (key, value) {
            //     console.log(value.user);
            //     candidates = value.user;
            // })
            candidates = response;
        },
        error: function (error) {
            console.log(error)
        }
    });


    $('input#nope').autocompleter({
        // marker for autocomplete matches
        highlightMatches: true,

        // object to local or url to remote search
        source: candidates,

        // show hint
        hint: true,

        // abort source if empty field
        empty: false,

        // max results
        limit: 5,

        callback: function (value, index, selected) {
            if (selected) {
                $('.icon').css('background-color', selected.hex);
            }
        }
    });
})


}); // end jQuery