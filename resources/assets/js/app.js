
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

 // window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('example-component', require('./components/ExampleComponent.vue'));

// const app = new Vue({
//     el: '#app'
// });
//

// jQuery
import $ from 'jquery';
window.$ = window.jQuery = $;

//Moment
window.moment = require('moment');

// jQuery-ui autocomplete widget
import 'jquery-ui/ui/widgets/autocomplete.js';

// Fontawesome
import '@fortawesome/fontawesome-free/js/all.js';

//fullcalendar
import 'fullcalendar/dist/fullcalendar.min';

//main.js
import "./main.js";

//tempusdominus-bootstrap-4
require('tempusdominus-bootstrap-4');
//datetimepicker default settings
$.fn.datetimepicker.Constructor.Default = $.extend({}, $.fn.datetimepicker.Constructor.Default, {
    locale: 'ru',
    sideBySide: true,
    icons: {
        time: 'far fa-clock',
        date: 'far fa-calendar-alt',
        up: 'fas fa-arrow-up',
        down: 'fas fa-arrow-down',
        previous: 'fas fa-chevron-left',
        next: 'fas fa-chevron-right',
        today: 'far fa-calendar-check',
        clear: 'far fa-trash-alt',
        close: 'fas fa-times'
    }
});
