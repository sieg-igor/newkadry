@extends('layouts.app')

@section('content')

<div class="row justify-content-center">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                Предстоящие интервью
                <a href="{{ route('home') }}" class="close">
                    <span aria-hidden="true">&times;</span>
                </a>
            </div>

            <div class="card-body">
                @can('manage-users')
                    <p><a href="{{ route('interview.create') }}" class="btn btn-success">Добавить новое</a></p>
                @endcan
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>ФИО</th>
                        <th>Фото</th>
                        <th>Позиция</th>
                        <th>Город</th>
                        <th>Дата</th>
                        <th>Время</th>
                        <th>Опции</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?//php echo date('d-m-Y', time()) ?>
                    @foreach ($interviews as $interview)
                        <?php $today = $interview['date_from'] === date('Y-m-d') ?>
                        <tr></tr>
                        <tr style="{{ $today ? 'background:#F9E8E8' : 'background:white'  }}">
                            <td>
                                <a href="{{ route('user.show', $users[$interview['candidate_id']]['id']) }}">
                                    {{$users[$interview['candidate_id']]['first_name'] or 'FIRST_NAME_IS_NOT_DEFINED'}}
                                    {{$users[$interview['candidate_id']]['second_name'] or 'SECOND_NAME_IS_NOT_DEFINED' }}
                                    {{$users[$interview['candidate_id']]['last_name'] or 'LAST_NAME_IS_NOT_DEFINED' }}
                                </a>
                                <br>
                                <span style="font-size: 12px">{{ $users[$interview['candidate_id']]['phone'] or 'Phone is not set' }}</span>
                            </td>
                            <td>
                                <img src="storage/{{$users[$interview['candidate_id']]['photo'] or 'Photo is not set'}}" class="img rounded-circle" width="60px" alt="Photo is not set"/>
                            </td>
                            <td>
                                {{$positions[$interview['position_id']]['title'] or 'POSITION_IS_NOT_DEFINED'}}
                                <br> <span style="font-size: 10px">Присутствуют:</span>

                                @foreach($interview->interviewEmployee as $interviewEmployee)
                                    <span class="badge badge-pill badge-light">
                                        <i class="fas fa-times interviewer-remove interviewer-{{$interviewEmployee->user_id or 'USER_ID_IS_NOT_DEFINED'}}" style="cursor:pointer;" data-interviewer_id="{{$interviewEmployee->user_id or 'USER_ID_IS_NOT_DEFINED'}}"></i>
                                            {{ \App\Entities\User::getUserById($interviewEmployee->user_id) ?? '' }}
                                    </span>
                                @endforeach
                            </td>
                            <td>{{$cities[$interview['city_id']]['title'] or 'CITY_IS_NOT_DEFINED'}}</td>
                            <td>{{date('d.m.Y', strtotime($interview->date_from))}} <br>
                                <?php echo $today ? '<span class="badge badge-danger">СЕГОДНЯ</span>' : '' ?>
                            </td>
                            <td>
                                {{date('H:i', strtotime($interview->date_from))}}
                                <?php $left = (int)(((strtotime($interview->date_from) - time())) / 60) ?>
                                <!-- +3 часа к нашей зоне -->
                                    @if($left < 60 && $left > 0)
                                        <i style="color:red" class="fa fa-bell" aria-hidden="true"></i>
                                        <span class="badge badge-pill badge-success">Через {{ $left }} мин</span>
                                    @elseif($left < 15 && $left > 0)
                                        <br>
                                        <span class="badge badge-pill badge-success">Через {{ $left }} мин</span>
                                    @elseif($left > -30 && $left <= 0)
                                        <br>
                                        <span class="badge badge-pill badge-danger">Идёт собеседование</span>
                                    @elseif($left <= -30)
                                        <br>
                                        <span class="badge badge-pill badge-secondary">Cобеседование прошло</span>
                                    @endif
                            </td>
                            <td>
                                @can('manage-users')
                                    <div class="d-flex flex-row">
                                        <a href="{{ route('interview.show', $interview ?? '') }}" class="btn btn-sm btn-primary mr-1">Детали</a>
                                        <form method="POST" action="{{ route('interview.destroy', $interview ?? '') }}">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-sm btn-danger mr-1">Удалить</button>
                                        </form>
                                    </div>
                                @endcan
                            </td>
                            {{--<td><span class="badge badge-primary">{{$statusesList[$user->status]}}</span></td>
                            <td>
                                @if($user->photo)
                                    <img class="little-photo" src="{{ asset('storage/' . $user->photo) }}" />
                                @endif
                            </td>
                            <td>
                                @can('manage-users')
                                    <div class="d-flex flex-row">
                                        <a href="{{ route('user.show', $user) }}" class="btn btn-sm btn-primary mr-1">Детали</a>
                                        <form method="POST" action="{{ route('user.destroy', $user) }}">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-sm btn-danger mr-1">Удалить</button>
                                        </form>
                                    </div>
                                @endcan
                            </td>--}}
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{ $interviews->links() }}
            </div>
        </div>
    </div>
</div>


@endsection

