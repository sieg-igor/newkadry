{{--@dd($interview)--}}

<div class="form-group">
    <input type="hidden" value="{{$interview->id}}" name="interview_id">
    <label for="exampleFormControlSelect1">Кандидат</label>
    <select id="candidate" class="form-control{{ $errors->has('candidate_id') ? ' is-invalid' : '' }}" name="candidate_id">
        <option value="{{$interview->user->id}}">{{$interview->user->first_name}} {{$interview->user->last_name}}</option>

        {{--{{ $i = 0 }}
        @foreach ($data->candidatesList as $key => $value)
            <option value="{{ $data->candidatesList[$i]->user->id }}"{{ $key == old('position') ? ' selected' : '' }}>
                {{ $data->candidatesList[$i]->user->first_name }}
                {{ $data->candidatesList[$i]->user->second_name }}
                {{ $data->candidatesList[$i]->user->last_name }}
                &nbsp;&nbsp;
                ({{ $data->candidatesList[$i]->city->title }},&nbsp;
                {{ $data->candidatesList[$i]->position->title }})
            </option>
            {{ $i++ }}
        @endforeach--}}

    </select>
    @if ($errors->has('candidate_id'))
        <span class="invalid-feedback"><strong>{{ $errors->first('candidate_id') }}</strong></span>
    @endif
</div>
<div class="form-group">
    <label for="exampleFormControlSelect1">Позиция</label>
    <select id="position" class="form-control{{ $errors->has('position_id') ? ' is-invalid' : '' }}" name="position_id">
        @foreach ($data->positionsList as $key => $value)
            <option value="{{ $key }}"{{ $key == $interview->position_id ? ' selected' : '' }}>
                {{ $value }}
            </option>
        @endforeach
    </select>
    @if ($errors->has('position_id'))
        <span class="invalid-feedback"><strong>{{ $errors->first('position_id') }}</strong></span>
    @endif
</div>
<div class="form-group">
    <label for="exampleFormControlSelect1">Город</label>
    <select  id="city_id" name="city_id" class="form-control{{ $errors->has('city_id') ? ' is-invalid' : '' }}">
        @foreach ($data->citiesList as $key => $value)
            <option value="{{ $key }}"{{ $key == $interview->city_id ? ' selected' : '' }}>
                {{ $value }}
            </option>
        @endforeach
    </select>
    @if ($errors->has('city_id'))
        <span class="invalid-feedback"><strong>{{ $errors->first('city_id') }}</strong></span>
    @endif
</div>
<div class="form-group">
    <label for="exampleFormControlSelect1">Вакансия</label>
    <select id="vacancy_id" name="vacancy_id" class="form-control{{ $errors->has('vacancy_id') ? ' is-invalid' : '' }}" name="vacancy_id">
        <option value="{{$interview->vacancy[0]->id}}">{{$interview->vacancy[0]->title}}</option>
        @foreach ($data->vacanciesList as $key => $value)
            <option value="{{ $key }}"{{ $key == old('position') ? ' selected' : '' }}>
                {{ $value }}
            </option>
        @endforeach
    </select>
    @if ($errors->has('vacancy_id'))
        <span class="invalid-feedback"><strong>{{ $errors->first('vacancy_id') }}</strong></span>
    @endif
</div>

<div class="form-group">
    <label for="exampleFormControlSelect2">Наши участники интервью</label>
    <br>

{{--@dd($allInterviewersIDArray);--}}

    @for ($j = 0; $j < count($data->usersHRList); $j++)
<!--        --><?php //echo in_array($data->usersHRList[$j]->id, $allInterviewersIDArray) ?  ' checked' : '' ?>
        <input type="checkbox"
               data-user_id="{{ $data->usersHRList[$j]->id }}"
               data-intreviewer_id="{{ $interview->interviewEmployee[$j]->user_id ?? ''}}"
               {{in_array($data->usersHRList[$j]->id, $allInterviewersIDArray) ?  ' checked' : ''}}
        > &nbsp;
            {{ $data->usersHRList[$j]->first_name  }}
            {{ $data->usersHRList[$j]->last_name }}

        <br>
    @endfor
<br>
{{--@endfor--}}
</div>
<div class="form-group">
    <label for="calendar_event_begin">Дата интервью</label>
    <div class='input-group date' id='begin-datetimepicker' data-target-input="nearest">
        @if ($errors->has('start'))
            <div class="alert alert-danger">{{ $errors->first('start') }}</div>
        @endif
        <input type="text" class="form-control datetimepicker-input" data-target="#begin-datetimepicker" value="{{date('d.m.Y H:i', strtotime($interview->date_from))}}">
        <div class="input-group-append" data-target="#begin-datetimepicker" data-toggle="datetimepicker">
            <div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
        </div>
    </div>
    <input type="hidden" name="date_from" class="form-control" id="calendar_event_begin" value="{{date('Y-m-d H:i:s', strtotime($interview->date_from))}}">
</div>

{{--<div class="row">--}}
{{--<div class="col">--}}
    {{--<label for="date">Дата интервью</label>--}}
    {{--<input id="date" type="date" class="form-control" name="date_from" value="{{$interview->date_from}}" placeholder="Изменяемая дата: {{$interview->date_from}}">--}}
{{--</div>--}}
{{--<div class="col">--}}
    {{--<label for="time_begin">Время начала</label>--}}
    {{--<input id="time" type="text" class="form-control" name="date_to" value="{{$interview->date_to}}">--}}
{{--</div>--}}
{{--</div>--}}

{{--<h3>Inline</h3>
<div id="date_picker"> </div>
<script type="text/javascript">
$(function(){
    $('#date_picker').dtpicker();
});
</script>--}}
<script>
    $(function () {
        $('#begin-datetimepicker').datetimepicker({
        });
        $('#begin-datetimepicker').on('change.datetimepicker', function (e) {
            $('input#calendar_event_begin').val(window.moment(e.date).format('YYYY-MM-DD HH:mm:ss'));
        });


    });
</script>
