@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    {{--@if($user->isCandidate())--}}
                        <a href="{{ route('candidate.index') }}" class="close">
                            {{--@else--}}
                                <a href="{{ route('user.index') }}" class="close">
                                    {{--@endif--}}
                                    <span aria-hidden="true">&times;</span>
                                </a>
                        </a>
                                <h3>
{{--                                    {{ $userStatusesList[$user->status] }}--}}
{{--                                    {{$user->first_name . ' ' . $user->last_name}}--}}
                                </h3>
                </div>
                <div class="card-body">
                    <div class="row">
{{--                        <div class="@if ($user->isCandidate()) {{ 'col-md-5' }} @else {{ 'col-md-12' }} @endif">--}}
                            <h4>Основные данные</h4>
                        <p></p>
                            @can('manage-users')
                                <div class="btn-group">
                                    {{--<button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Изменить статус пользователя
                                    </button>
                                    <div class="dropdown-menu">
                                        @foreach ($userStatusesList as $key => $value)
                                            @if ($user->status !== $key)
                                                <form method="POST" action="{{ route('user.changeStatus', ['user' => $user, 'status' => $key]) }}" class="mr-1">
                                                    @csrf
                                                    <button class="dropdown-item">{{ $value }}</button>
                                                </form>
                                            @endif
                                        @endforeach
                                    </div>--}}
                                   {{-- <button type="button" class="btn btn-danger" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="margin-left: 30px; margin-bottom: 5px">
                                        Редактировать интервью
                                    </button>--}}
                                </div>
                            @endcan
                            {{--@if (Gate::allows('edit-user-data', $user))--}}
                                <a href="{{ route('interview.edit', $interview_id) }}" class="btn btn-danger" style="margin-bottom: 5px; margin-left: 30px;">Редактировать</a>
                                {{--@endif--}}
                                </p>
                                <table class="table table-bordered table-striped">
                                    <tbody>
                                    <tr>
                                        <th>Полное имя</th><td>{{ $interview[0]->user->first_name . ' ' . $interview[0]->user->last_name . ' ' . $interview[0]->user->second_name }}</td>
                                    </tr>
                                    <tr>
                                        <th>Город</th><td>{{ $interview[0]->city->title }}</td>
                                    </tr>
                                    <tr>
                                        <th>Должность</th><td>{{ $interview[0]->position->title }}</td>
                                    </tr>
                                    <tr>
                                        <th>Вакансия</th><td>{{ $interview[0]->vacancy[0]->title }}</td>
                                    </tr>
                                    <tr>
                                        <th>Участники от нас</th>
                                        <td>
                                            @foreach($interview[0]->interviewEmployee as $interviewEmployee)
                                                {{$interviewEmployee}}
                                            @endforeach
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Телефон</th><td>{{ $interview[0]->user->phone }}</td>
                                    </tr>
                                    <tr>
                                        <th>E-mail</th><td>{{ $interview[0]->user->email }}</td>
                                    </tr>
                                    <tr>
                                        <th>Ссылки на соцсети</th><td>{{ $interview[0]->user->socials }}</td>
                                    </tr>
                                    <tr>
                                        <th>Дата</th><td>{{ date('d.m.Y', strtotime($interview[0]->date_from))}}</td>
                                    </tr>
                                    <tr>
                                        <th>Время</th><td>{{ date('H:i', strtotime($interview[0]->date_from))}}</td>
                                    </tr>
                                    <tr>
                                        <th>Фото</th>
                                        <td>
                                            @if ($interview[0]->user->photo)
                                                <img class="little-photo img-circle" src="{{ asset('storage/' . $interview[0]->user->photo) }}" />
                                            @endif
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection