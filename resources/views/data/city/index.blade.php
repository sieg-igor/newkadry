@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Города
                    <a href="{{ route('home') }}" class="close">
                        <span aria-hidden="true">&times;</span>
                    </a>
                </div>
                <div class="card-body">
                    <p><a href="{{ route('data.city.create') }}" class="btn btn-success">Добавить город</a></p>

                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Название</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach ($cities as $city)
                            <tr>
                                <td>{{$city->title}}</td>
                                <td>
                                    <div class="d-flex flex-row">
                                        <a href="{{ route('data.city.edit', $city) }}" class="btn btn-sm btn-primary mr-1">Редактировать</a>
                                        <form method="POST" action="{{ route('data.city.destroy', $city) }}">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-sm btn-danger mr-1">Удалить</button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                    {{ $cities->links() }}
                </div>
            </div>
        </div>
    </div>

@endsection