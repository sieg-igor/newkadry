@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Должности
                    <a href="{{ route('home') }}" class="close">
                        <span aria-hidden="true">&times;</span>
                    </a>
                </div>
                <div class="card-body">
                    <p><a href="{{ route('data.position.create') }}" class="btn btn-success">Добавить должность</a></p>

                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Название</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach ($positions as $position)
                            <tr>
                                <td>{{$position->title}}</td>
                                <td>
                                    <div class="d-flex flex-row">
                                        <a href="{{ route('data.position.edit', $position) }}" class="btn btn-sm btn-primary mr-1">Редактировать</a>
                                        <form method="POST" action="{{ route('data.position.destroy', $position) }}">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-sm btn-danger mr-1">Удалить</button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                    {{ $positions->links() }}
                </div>
            </div>
        </div>
    </div>

@endsection