@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    Создать должность
                    <a href="{{ route('data.position.index') }}" class="close">
                        <span aria-hidden="true">&times;</span>
                    </a>
                </div>

                <div class="card-body">
                    <form method="POST" action="{{ route('data.position.store') }}">
                        @csrf

                        <div class="form-group">
                            <label for="title" class="col-form-label">Название должности</label>
                            <input id="title" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" value="{{ old('title') }}" required>
                            @if ($errors->has('title'))
                                <span class="invalid-feedback"><strong>{{ $errors->first('title') }}</strong></span>
                            @endif
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Создать</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection