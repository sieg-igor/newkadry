@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                Детали компании
                <a href="{{ route('data.company.index') }}" class="close">
                    <span aria-hidden="true">&times;</span>
                </a>
            </div>

            <div class="card-body">
                <table class="table table-bordered table-striped">
                    <tbody>
                        <tr>
                            <th>Название</th><td>{{ $company->title }}</td>
                        </tr>
                        <tr>
                            <th>Город</th><td>{{ $company->city->title}}</td>
                        </tr>
                        <tr>
                            <th>Описание</th><td>{{ $company->about }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection