@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Компании
                    <a href="{{ route('home') }}" class="close">
                        <span aria-hidden="true">&times;</span>
                    </a>
                </div>
                <div class="card-body">
                    <p><a href="{{ route('data.company.create') }}" class="btn btn-success">Добавить компанию</a></p>

                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Название</th>
                            <th>Город</th>
                            <th>Описание</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach ($companies as $company)
                            <tr>
                                <td><a href="{{ route('data.company.show', $company) }}">{{$company->title}}</a></td>
                                <td>{{$company->city->title}}</td>
                                <td>{{$company->about}}</td>
                                <td>
                                    <div class="d-flex flex-row">
                                        <a href="{{ route('data.company.edit', $company) }}" class="btn btn-sm btn-primary mr-1">Редактировать</a>
                                        <form method="POST" action="{{ route('data.company.destroy', $company) }}">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-sm btn-danger mr-1">Удалить</button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                    {{ $companies->links() }}
                </div>
            </div>
        </div>
    </div>

@endsection