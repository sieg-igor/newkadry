@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    Редактировать компанию
                    <a href="{{ route('data.company.index') }}" class="close">
                        <span aria-hidden="true">&times;</span>
                    </a>
                </div>

                <div class="card-body">
                    <form method="POST" action="{{ route('data.company.update', $company) }}">
                        @csrf
                        @method('PUT')

                        <div class="form-group">
                            <label for="title" class="col-form-label">Название компании</label>
                            <input id="title" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" value="{{ old('title', $company->title) }}" required>
                            @if ($errors->has('title'))
                                <span class="invalid-feedback"><strong>{{ $errors->first('title') }}</strong></span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="city" class="col-form-label">Город</label>
                            <select id="city" class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}" name="city" required>
                                <option value=""></option>
                                @foreach ($citiesList as $city)
                                    <option value="{{ $city->id }}"{{ $city->id == old('city', $company->city_id) ? ' selected' : '' }}>
                                        {{ $city->title }}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="about" class="col-form-label">Описание</label>
                            <textarea id="about" class="form-control{{ $errors->has('about') ? ' is-invalid' : '' }}" name="about" rows="2">{{ old('about', $company->about) }}</textarea>
                            @if ($errors->has('about'))
                                <span class="invalid-feedback"><strong>{{ $errors->first('about') }}</strong></span>
                            @endif
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Сохранить</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection