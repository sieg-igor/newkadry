@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    Создать доступ
                    <a href="{{ route('data.remote-access.index') }}" class="close">
                        <span aria-hidden="true">&times;</span>
                    </a>
                </div>

                <div class="card-body">
                    <form method="POST" action="{{ route('data.remote-access.store') }}">
                        @csrf

                        <div class="form-group">
                            <label for="url" class="col-form-label">URL</label>
                            <input id="url" class="form-control{{ $errors->has('url') ? ' is-invalid' : '' }}"
                                   name="url" value="{{ old('url') }}" required
                            >
                            @if ($errors->has('url'))
                                <span class="invalid-feedback"><strong>{{ $errors->first('url') }}</strong></span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="api_token" class="col-form-label">Api-Token</label>
                            <input id="api_token" class="form-control{{ $errors->has('api_token') ? ' is-invalid' : '' }}"
                                   name="api_token" value="{{ old('api_token') }}" required
                            >
                            @if ($errors->has('api_token'))
                                <span class="invalid-feedback"><strong>{{ $errors->first('api_token') }}</strong></span>
                            @endif
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Создать</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection