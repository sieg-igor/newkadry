@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Доступы к сайтам
                    <a href="{{ route('home') }}" class="close">
                        <span aria-hidden="true">&times;</span>
                    </a>
                </div>
                <div class="card-body">
                    <p><a href="{{ route('data.remote-access.create') }}" class="btn btn-success">Добавить доступ</a></p>

                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Url</th>
                            <th>Api-Token</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach ($accesses as $access)
                            <tr>
                                <td>{{$access->url}}</td>
                                <td>{{$access->api_token}}</td>
                                <td>
                                    <div class="d-flex flex-row">
                                        <a href="{{ route('data.remote-access.edit', $access) }}" class="btn btn-sm btn-primary mr-1">Редактировать</a>
                                        <form method="POST" action="{{ route('data.remote-access.destroy', $access) }}">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-sm btn-danger mr-1">Удалить</button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                    {{ $accesses->links() }}
                </div>
            </div>
        </div>
    </div>

@endsection