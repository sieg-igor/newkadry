@extends('layouts.app')
@section('styles')
    {{--<link rel="stylesheet" href="{!! asset('css/fullcalendar.min.css') !!}">--}}
@endsection

@section('content')
<center>
    <div class="container-fluid" style="margin: 0 auto;">
        <div class="col-md-7 col-md-offset-3">
            <div class="pull-right">
                <center>
                    <a href="{{route('calendar.add')}}" class="btn btn-primary add-shadows" id="create-event">
                        Добавить событие
                    </a>
                </center>
            </div>
        </div>
    </div>
</center>
    <div class="container-fluid" id="calendarIndex">
        <div class="col-md-12 col-md-offset-3">
            <!-- init a small calendar -->
            {!! $calendar->calendar() !!}
        </div>
    </div>

@endsection

@section('scripts')

    {!! $calendar->script() !!}


    <script>
        $(function () {
            $('.fc-day, .fc-day-top').click(function () {
                var date = $(this).data('date');
                window.location.href = '/?interval=' +date;
            });
        })
    </script>

@endsection
