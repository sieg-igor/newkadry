@extends('layouts.app')

@section('styles')

@endsection

@section('content')

    @if (session('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif


    <div class="col-md-3"></div>
    <div class="col-md-6">
        <h1>Новое событие</h1>
        <form action="{{route('calendar.create')}}" method="post">
            <input name="_method" type="hidden" value="PUT">
            @csrf
            <div class="form-group">
                <label for="calendar_title">Заголовок</label>
                @if ($errors->has('title'))
                    <div class="alert alert-danger">{{ $errors->first('title') }}</div>
                @endif
                <input type="text" name="title" class="form-control" id="" aria-describedby="emailHelp" placeholder="Кратко о событии">
                <small id="calendar_title_help" class="form-text text-muted"></small>
            </div>
            <div class="form-group">
                <label for="calendar_description">Текст события</label>
                @if ($errors->has('description'))
                    <div class="alert alert-danger">{{ $errors->first('description') }}</div>
                @endif
                <textarea class="form-control" name="description" id="calendar_description" placeholder="Описание события..." rows="10"></textarea>
            </div>
            <div class="form-group">
                <label for="calendar_event_begin">Начало события</label>
                <div class='input-group date' id='begin-datetimepicker' data-target-input="nearest">
                    @if ($errors->has('start'))
                        <div class="alert alert-danger">{{ $errors->first('start') }}</div>
                    @endif
                    <input type="text" class="form-control datetimepicker-input" data-target="#begin-datetimepicker" value="<?=date('d.m.Y H:i')?>">
                        <div class="input-group-append" data-target="#begin-datetimepicker" data-toggle="datetimepicker">
                            <div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
                        </div>
                </div>
                <input type="hidden" name="start" class="form-control" id="calendar_event_begin" value="<?=date('Y-m-d H:i:s')?>">
            </div>

            <div class="form-group">
                <label for="calendar_event_end">Конец события</label>
                <div class='input-group date' id='end-datetimepicker' data-target-input="nearest">
                    @if ($errors->has('end'))
                        <div class="alert alert-danger">{{ $errors->first('end') }}</div>
                    @endif
                    <input type="text" class="form-control datetimepicker-input" data-target="#end-datetimepicker" value="<?=date('d.m.Y H:i')?>">
                        <div class="input-group-append" data-target="#end-datetimepicker" data-toggle="datetimepicker">
                            <div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
                        </div>
                </div>
                <input type="hidden" name="end" class="form-control" id="calendar_event_end"  value="<?=date('Y-m-d H:i:s')?>">
            </div>
            <div class="form-group">
                <label for="calendar_event_color">Цвет события</label>
                <input type="color" name="color" class="form-control" id="calendar_event_color">
            </div>

            <button type="submit" class="btn btn-primary">Добавить</button>
        </form>
    </div>
    <div class="col-md-3"></div>



@endsection

@section('scripts')
    <script>
        $(function () {
           $('#begin-datetimepicker').datetimepicker({
           });
           $('#begin-datetimepicker').on('change.datetimepicker', function (e) {
              $('input#calendar_event_begin').val(window.moment(e.date).format('YYYY-MM-DD HH:mm:ss'));
           });
           
            $('#end-datetimepicker').datetimepicker({
            });
            $('#end-datetimepicker').on('change.datetimepicker', function (e) {
                $('input#calendar_event_end').val(window.moment(e.date).format('YYYY-MM-DD HH:mm:ss'));
            });


        });
    </script>
@endsection

