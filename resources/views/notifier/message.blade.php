Напоминание, на {{$interview->date_from}} в {{$interview->date_to}} состоится собеседование.
Имя : {{$interview->user->first_name}} {{$interview->user->last_name}} {{$interview->user->second_name}};
Должность: {{$interview->position->title}} , вакансия {{$interview->vacancy[0]->title}} , город {{$interview->city->title}};
Контакты: Телефон - {{$interview->user->phone}}, E-mail - {{$interview->user->email}}, Ссылки на соцсети - {{$interview->user->socials}}