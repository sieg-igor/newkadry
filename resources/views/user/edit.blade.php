@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    Редактирование пользователя
                    <a href="{{ route('user.show', $user) }}" class="close">
                        <span aria-hidden="true">&times;</span>
                    </a>
                </div>

                <div class="card-body">
                    <form method="POST" action="{{ route('user.update', $user) }}" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')

                        <div class="form-group">
                            <label for="first_name" class="col-form-label required">Имя</label>
                            <input id="first_name" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{ old('first_name', $user->first_name) }}" required>
                            @if ($errors->has('first_name'))
                                <span class="invalid-feedback"><strong>{{ $errors->first('first_name') }}</strong></span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="last_name" class="col-form-label required">Фамилия</label>
                            <input id="last_name" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{ old('last_name', $user->last_name) }}" required>
                            @if ($errors->has('last_name'))
                                <span class="invalid-feedback"><strong>{{ $errors->first('last_name') }}</strong></span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="second_name" class="col-form-label">Отчество</label>
                            <input id="second_name" class="form-control{{ $errors->has('second_name') ? ' is-invalid' : '' }}" name="second_name" value="{{ old('second_name', $user->second_name) }}">
                            @if ($errors->has('second_name'))
                                <span class="invalid-feedback"><strong>{{ $errors->first('second_name') }}</strong></span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="email" class="col-form-label required">E-Mail</label>
                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email', $user->email) }}" required>
                            @if ($errors->has('email'))
                                <span class="invalid-feedback"><strong>{{ $errors->first('email') }}</strong></span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="socials" class="col-form-label">Ссылки на соцсети</label>
                            <textarea id="socials" class="form-control{{ $errors->has('socials') ? ' is-invalid' : '' }}" name="socials" rows="2">{{ old('socials', $user->socials) }}</textarea>
                            @if ($errors->has('socials'))
                                <span class="invalid-feedback"><strong>{{ $errors->first('socials') }}</strong></span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="education" class="col-form-label">Образование</label>
                            <textarea id="education" class="form-control{{ $errors->has('education') ? ' is-invalid' : '' }}" name="education" rows="2">{{ old('education', $user->education) }}</textarea>
                            @if ($errors->has('education'))
                                <span class="invalid-feedback"><strong>{{ $errors->first('education') }}</strong></span>
                            @endif
                        </div>

                        <div class="form-group">
                            @if ($user->photo)
                                <img class="little-photo" src="{{ asset('storage/' . $user->photo) }}" />
                            @endif
                            <label for="photo" class="col-form-label">Фото</label>
                            <input id="photo" type="file" class="form-control{{ $errors->has('photo') ? ' is-invalid' : '' }}" name="photo">
                            @if ($errors->has('photo'))
                                <span class="invalid-feedback"><strong>{{ $errors->first('photo') }}</strong></span>
                            @endif
                        </div>


                        <div class="form-group">
                            <label for="skype" class="col-form-label">Skype</label>
                            <textarea id="skype" class="form-control{{ $errors->has('skype') ? ' is-invalid' : '' }}" name="skype" rows="2" value="{{ old('skype', $user->skype) }}">{{ $user->skype }}</textarea>
                            @if ($errors->has('skype'))
                                <span class="invalid-feedback"><strong>{{ $errors->first('skype') }}</strong></span>
                            @endif
                        </div>


                        <div class="form-group">
                            <label for="telegram" class="col-form-label">Телеграм</label>
                            <textarea id="telegram" class="form-control{{ $errors->has('telegram') ? ' is-invalid' : '' }}" name="telegram" rows="2" value="{{ old('telegram', $user->telegram) }}">{{ $user->telegram }}</textarea>
                            @if ($errors->has('telegram'))
                                <span class="invalid-feedback"><strong>{{ $errors->first('telegram') }}</strong></span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="phone" class="col-form-label">Телефон</label>
                            <textarea id="phone" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" rows="2" value="{{ old('phone', $user->phone) }}">{{ $user->phone }}</textarea>
                            @if ($errors->has('phone'))
                                <span class="invalid-feedback"><strong>{{ $errors->first('phone') }}</strong></span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="slack" class="col-form-label">Слак</label>
                            <textarea id="slack" class="form-control{{ $errors->has('slack') ? ' is-invalid' : '' }}" name="slack" rows="2" value="{{ old('slack', $user->slack) }}">{{ $user->slack }}</textarea>
                            @if ($errors->has('slack'))
                                <span class="invalid-feedback"><strong>{{ $errors->first('slack') }}</strong></span>
                            @endif
                        </div>


                        <div class="form-group">
                            <label class="col-form-label">Нотификации через</label>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox"
                                       name="notify_with[]"
                                       value="email"
                                       id="notify-email"
                                       @if(in_array('email', old('notify_with', (array)$user->notify_with)))
                                           checked="checked"
                                       @endif
                                >
                                <label class="form-check-label" for="notify-email">
                                    Имейл
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="notify_with[]" value="slack" id="notify-slack"
                                    @if(in_array('slack', old('notify_with', (array)$user->notify_with)))
                                        checked="checked"
                                    @endif
                                >
                                <label class="form-check-label" for="notify-slack">
                                    Слак
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="notify_with[]" value="telegram" id="notify-telegram"
                                    @if(in_array('telegram', old('notify_with', (array)$user->notify_with)))
                                       checked="checked"
                                    @endif
                                >
                                <label class="form-check-label" for="notify-telegram">
                                    Телеграм
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="notify_with[]" value="skype" id="notify-skype"
                                    @if(in_array('skype', old('notify_with', (array)$user->notify_with)))
                                       checked="checked"
                                    @endif
                                >
                                <label class="form-check-label" for="notify-skype">
                                    Скайп
                                </label>
                            </div>
                        </div>
                        @can('manage-users')
                        <div class="form-group">
                            <label for="employer_at">Сотрудник от</label>
                            <div class='input-group date' id='employer-datetimepicker' data-target-input="nearest">
                                @if ($errors->has('employer_at'))
                                    <div class="alert alert-danger">{{ $errors->first('employer_at') }}</div>
                                @endif
                                <input type="text" id="employer_at_datetimepicker" class="form-control datetimepicker-input" data-target="#employer-datetimepicker"
                                       value="{{$user->employer_at != null ? date('d.m.Y H:i', strtotime($user->employer_at)) : ''}}"
                                >
                                <div class="input-group-append" data-target="#employer-datetimepicker" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
                                </div>
                                <a id="set_employer" class="btn btn-group-sm btn-outline-light">Установить?</a>
                            </div>
                            <input type="hidden" name="employer_at" class="form-control" id="employer_at" value="{{$user->employer_at}}">
                        </div>

                        <div class="form-group">
                            <label for="fired_at">Уволен от</label>
                            <div class='input-group date' id='fired-datetimepicker' data-target-input="nearest">
                                @if ($errors->has('fired_at'))
                                    <div class="alert alert-danger">{{ $errors->first('fired_at') }}</div>
                                @endif
                                <input type="text" id="fired_at_datetimepicker" class="form-control datetimepicker-input" data-target="#fired-datetimepicker"
                                       value="{{$user->fired_at != null ? date('d.m.Y H:i', strtotime($user->fired_at)) : ''}}"
                                >
                                <div class="input-group-append" data-target="#fired-datetimepicker" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
                                </div>
                                <a id="set_fired" class="btn btn-group-sm btn-outline-light" >Установить?</a>
                            </div>
                            <input type="hidden" name="fired_at" class="form-control" id="fired_at"  value="{{$user->fired_at}}">
                        </div>
                        @endcan
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Сохранить</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        $(function () {
            $('#employer-datetimepicker').datetimepicker({});

            $('#employer-datetimepicker').on('change.datetimepicker', function (e) {
                $('#set_employer').html('Установить?');
                 $('#set_employer').on('click', function () {
                     if($('#employer_at_datetimepicker').val()!=='') {
                         $('input#employer_at').val(window.moment(e.date).format('YYYY-MM-DD HH:mm:ss'));
                         $('#set_employer').html('Установлено!');
                     }else {
                         $('input#employer_at').val('');
                         $('#set_employer').html('Установлено!');
                     }
                 });
            });

            $('#fired-datetimepicker').datetimepicker({});

            $('#fired-datetimepicker').on('change.datetimepicker', function (e) {
                $('#set_fired').html('Установить?');
                $('#set_fired').on('click', function () {
                    if($('#fired_at_datetimepicker').val()!=='') {
                        $('input#fired_at').val(window.moment(e.date).format('YYYY-MM-DD HH:mm:ss'));
                        $('#set_fired').html('Установлено!');
                    }else {
                        $('input#fired_at').val('');
                        $('#set_fired').html('Установлено!');
                    }
                });
            });

        });
    </script>
@endsection