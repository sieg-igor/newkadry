<?php use App\Entities\Candidate; ?>

@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                @if($user->isCandidate())
                    <a href="{{ route('candidate.index') }}" class="close">
                        <span aria-hidden="true">&times;</span>
                    </a>
                @else
                    <a href="{{ route('user.index') }}" class="close">
                        <span aria-hidden="true">&times;</span>
                    </a>
                @endif
                <h3>
                    {{ $userStatusesList[$user->status] }}
                    {{$user->first_name . ' ' . $user->last_name}}
                </h3>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="@if ($user->isCandidate()) {{ 'col-md-5' }} @else {{ 'col-md-12' }} @endif">
                        <h4>Основные данные</h4>
                        <p>
                            @can('manage-users')
                                <div class="btn-group">
                                    <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Изменить статус пользователя
                                    </button>
                                    <div class="dropdown-menu">
                                        @foreach ($userStatusesList as $key => $value)
                                            @if ($user->status !== $key)
                                                <form method="POST" action="{{ route('user.changeStatus', ['user' => $user, 'status' => $key]) }}" class="mr-1">
                                                    @csrf
                                                    <button class="dropdown-item">{{ $value }}</button>
                                                </form>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            @endcan
                            @if (Gate::allows('edit-user-data', $user))
                                <a href="{{ route('user.edit', $user) }}" class="btn btn-success">Редактировать</a>
                            @endif
                        <p></p>
                        <table class="table table-bordered table-striped">
                            <tbody>
                                <tr>
                                    <th>Полное имя</th><td>{{ $user->first_name  . ' ' . $user->second_name . ' ' . $user->last_name }}</td>
                                </tr>
                                <tr>
                                    <th>Статус</th><td><span class="badge badge-{{Candidate::getBadgeColor($userStatusesList[$user->status])}}">{{ $userStatusesList[$user->status] }}</span></td>
                                </tr>
                                <tr>
                                    <th>Email</th><td>{{ $user->email }}</td>
                                </tr>
                                <tr>
                                    <th>Ссылки на соцсети</th><td>{{ $user->socials }}</td>
                                </tr>
                                <tr>
                                    <th>Образование</th><td>{{ $user->education }}</td>
                                </tr>
                                <tr>
                                    <th>Сотрудник от</th><td>{{ $user->employer_at ? date('d.m.Y H:i', strtotime( $user->employer_at)) : ''}}</td>
                                </tr>
                                <tr>
                                    @if($user->fired_at != null)
                                        <th>Уволен от</th><td>{{ $user->fired_at ? date('d.m.Y H:i', strtotime( $user->fired_at)) : ''}}</td>
                                    @endif
                                </tr>
                                <tr>
                                    <th>Фото</th>
                                    <td>
                                        @if ($user->photo)
                                            <img class="little-photo" src="{{ asset('storage/' . $user->photo) }}" />
                                        @endif
                                    </td>
                                </tr>
                            <tr>
                                <th>Телефон</th><td>{{ $user->phone }}</td>
                            </tr>
                            <tr>
                                <th>Skype</th>
                                <td>
                                    {{ $user->skype }}
                                    @if( !empty( env('INTERVIEW_NOTIFICATOR_SKYPE_BOT_ADD_URL') ) )
                                        <a target="_blank" href="{{env('INTERVIEW_NOTIFICATOR_SKYPE_BOT_ADD_URL')}}">Добавить Бота</a>
                                    @endif
                                </td>

                            </tr>
                            <tr>
                                <th>Телеграм</th>
                                <td>
                                    {{ $user->telegram }}
                                    @if( !empty( env('INTERVIEW_NOTIFICATOR_TELEGRAM_BOT') ) )
                                        Bot ({{env('INTERVIEW_NOTIFICATOR_TELEGRAM_BOT')}})
                                    @endif
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    @if ($user->isCandidate())
                        <div class="col-md-7">
                            <h4>История кандидатства</h4>
                            @can('manage-users')
                                <p><a href="{{ route('candidate.createCandidateRecord', $user) }}" class="btn btn-success">Добавить</a></p>
                            @endcan
                            <div class="accordion" id="accordionHistory">
                                @php($first = true)
                                @foreach ($user->candidatesHistory as $candidate)
                                <div class="card">
                                    <div class="card-header" id="{{ $candidate->id }}">
                                        <h5 class="mb-1">
                                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse{{ $candidate->id }}" aria-expanded="true" aria-controls="collapse{{ $candidate->id }}">
                                                {{ $candidate->created_at }}
                                            </button>
                                        </h5>
                                    </div>

                                    <div id="collapse{{ $candidate->id }}" class="collapse @if ($first) show @endif" aria-labelledby="{{ $candidate->id }}" data-parent="#accordionHistory">
                                        <div class="card-body">
                                            @if ($first)
                                                @can('manage-users')
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            Изменить статус кандидата
                                                        </button>
                                                        <div class="dropdown-menu">
                                                            @foreach ($statusesList as $key => $value)
                                                                @if ($candidate->status !== $key)
                                                                    <form method="POST" action="{{ route('candidate.changeStatus', ['candidate' => $candidate, 'status' => $key]) }}" class="mr-1">
                                                                        @csrf
                                                                        <button class="dropdown-item">{{ $value }}</button>
                                                                    </form>
                                                                @endif
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                    <a href="{{ route('candidate.edit', $candidate) }}" class="btn btn-success">Редактировать</a>
                                                @endcan
                                            @endif
                                            <table class="table table-bordered table-striped">
                                                <tbody>
                                                    <tr>
                                                        <th>Статус</th><td><span class="badge badge-{{\App\Entities\Candidate::getBadgeColor($statusesList[$candidate->status])}}">{{ $statusesList[$candidate->status] }}</span></td>
                                                    </tr>
                                                    <tr>
                                                        <th>Рекрутер</th><td>{{ $candidate->recruiter->last_name . ' ' . $candidate->recruiter->first_name }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Должность</th><td>{{ $candidate->position_id ? $candidate->position->title : ''}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Зарплата</th><td>{{ $candidate->salary . ' ' . $candidate->currency->title }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Город</th><td>{{ $candidate->city->title }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Ссылка на резюме</th><td>{{ $candidate->cv_src }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Файл резюме</th>
                                                        <td>
                                                            @if ($candidate->cv_file)
                                                                <a target="_blank" href="{{ asset('storage/' . $candidate->cv_file) }}">скачать</a>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>Языки</th><td>{{ $candidate->languages }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Описание</th><td>{{ $candidate->description }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Последняя компания</th><td>{{ $candidate->last_company_id ? $candidate->lastCompany->title : ''}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Заметки</th><td>{{ $candidate->notes }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Дата добавления</th><td>{{ $candidate->created_at }}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                @php($first = false)
                                @endforeach
                            </div>
                        </div>
                    @endif
                </div>

            </div>
        </div>
    </div>
</div>
@endsection