<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label for="first_name" class="col-form-label required">Имя</label>
            <input id="first_name" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{ old('first_name') }}" required>
            @if ($errors->has('first_name'))
                <span class="invalid-feedback"><strong>{{ $errors->first('first_name') }}</strong></span>
            @endif
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="last_name" class="col-form-label required">Фамилия</label>
            <input id="last_name" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{ old('last_name') }}" required>
            @if ($errors->has('last_name'))
                <span class="invalid-feedback"><strong>{{ $errors->first('last_name') }}</strong></span>
            @endif
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="second_name" class="col-form-label">Отчество</label>
            <input id="second_name" class="form-control{{ $errors->has('second_name') ? ' is-invalid' : '' }}" name="second_name" value="{{ old('second_name') }}">
            @if ($errors->has('second_name'))
                <span class="invalid-feedback"><strong>{{ $errors->first('second_name') }}</strong></span>
            @endif
        </div>
    </div>
</div>

<div class="form-group">
    <label for="email" class="col-form-label required">E-Mail адрес</label>
    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
    @if ($errors->has('email'))
        <span class="invalid-feedback"><strong>{{ $errors->first('email') }}</strong></span>
    @endif
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="photo" class="col-form-label">Фото</label>
            <input id="photo" type="file" class="form-control{{ $errors->has('photo') ? ' is-invalid' : '' }}" name="photo" value="{{ old('photo') }}">
            @if ($errors->has('photo'))
                <span class="invalid-feedback"><strong>{{ $errors->first('photo') }}</strong></span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="socials" class="col-form-label">Ссылки на соцсети</label>
            <textarea id="socials" class="form-control{{ $errors->has('socials') ? ' is-invalid' : '' }}" name="socials" rows="2">{{ old('socials') }}</textarea>
            @if ($errors->has('socials'))
                <span class="invalid-feedback"><strong>{{ $errors->first('socials') }}</strong></span>
            @endif
        </div>
    </div>
</div>

<div class="form-group">
    <label for="education" class="col-form-label">Образование</label>
    <textarea id="education" class="form-control{{ $errors->has('education') ? ' is-invalid' : '' }}" name="education" rows="2">{{ old('education') }}</textarea>
    @if ($errors->has('education'))
        <span class="invalid-feedback"><strong>{{ $errors->first('education') }}</strong></span>
    @endif
</div>


<div class="form-group">
    <label for="skype" class="col-form-label">Skype</label>
    <textarea id="skype" class="form-control{{ $errors->has('skype') ? ' is-invalid' : '' }}" name="skype" rows="2">{{ old('skype') }}</textarea>
    @if ($errors->has('skype'))
        <span class="invalid-feedback"><strong>{{ $errors->first('skype') }}</strong></span>
    @endif
</div>


<div class="form-group">
    <label for="phone" class="col-form-label">Телефон</label>
    <textarea id="phone" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" rows="2">{{ old('phone') }}</textarea>
    @if ($errors->has('phone'))
        <span class="invalid-feedback"><strong>{{ $errors->first('phone') }}</strong></span>
    @endif
</div>

<div class="form-group">
    <label for="telegram" class="col-form-label">Телеграм</label>
    <textarea id="telegram" class="form-control{{ $errors->has('telegram') ? ' is-invalid' : '' }}" name="telegram" rows="2">{{ old('telegram') }}</textarea>
    @if ($errors->has('telegram'))
        <span class="invalid-feedback"><strong>{{ $errors->first('telegram') }}</strong></span>
    @endif
</div>

<div class="form-group">
    <label for="slack" class="col-form-label">Слак</label>
    <textarea id="slack" class="form-control{{ $errors->has('slack') ? ' is-invalid' : '' }}" name="slack" rows="2">{{ old('slack') }}</textarea>
    @if ($errors->has('slack'))
        <span class="invalid-feedback"><strong>{{ $errors->first('slack') }}</strong></span>
    @endif
</div>

