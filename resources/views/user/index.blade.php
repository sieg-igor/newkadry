@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header form-inline">
                    Сотрудники
                    <a href="#" class="btn dropdown-toggle" role="button" id="dropdownMenuLink" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false"
                    >
                        <i class="fa fa-fw fa-wrench"></i>
                        Фильтр сотрудников
                    </a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                        <a class="dropdown-item {{request()->query('fired') != "true" ? 'active': '' }}"
                           href="{{request()->fullUrlWithQuery(['fired' => "false"])}}"
                        >Действующие</a>
                        <a class="dropdown-item {{request()->query('fired') == "true" ? 'active': '' }}"
                           href="{{request()->fullUrlWithQuery(['fired' => "true"])}}"
                        >Уволеные</a>
                    </div>
                    <form action="{{request()->fullUrl()}}" method="GET" >
                        <input type="text" name="employer_search"
                               value="{{request()->query('employer_search')!=null ? request()->query('employer_search'): ''}}"
                        >
                        @if(request()->query('fired') == "true")
                            <input type='hidden' name='fired' value='true'/>
                        @endif
                        <button type="submit"><i class="fas fa-search"></i></button>
                    </form>
                    <a href="{{ route('home') }}" class="close" style="margin-left: 55%;">
                        <span aria-hidden="true">&times;</span>
                    </a>
                </div>

                <div class="card-body">
                    @can('manage-users')
                        <p><a href="{{ route('user.create') }}" class="btn btn-success">Добавить сотрудника</a></p>
                    @endcan
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            @if(request()->query('fired') == "true")
                            <th>Дата увольнения</th>
                            @else
                            <th>Дата принятия</th>
                            @endif
                            <th>Имя</th>
                            <th>E-mail</th>
                            <th>Статус</th>
                            <th>Фото</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($users as $user)
                            <tr>
                                @if(request()->query('fired') == "true")
                                    <td>{{$user->fired_at ? date('d.m.Y H:i', strtotime( $user->fired_at)) : ''}}</td>
                                @else
                                    <td>{{$user->employer_at ? date('d.m.Y H:i', strtotime( $user->employer_at)) : ''}}</td>
                                @endif
                                <td><a href="{{ route('user.show', $user->id) }}">{{$user->last_name . ' ' . $user->first_name . ' ' . $user->second_name}}</a></td>
                                <td>{{$user->email}}</td>
                                <td><span class="badge badge-primary">{{$statusesList[$user->status]}}</span></td>
                                <td>
                                    @if($user->photo)
                                        <img class="little-photo" src="{{ asset('storage/' . $user->photo) }}" />
                                    @endif
                                </td>
                                <td>
                                    @can('manage-users')
                                        <div class="d-flex flex-row">
                                            <a href="{{ route('user.show', $user) }}" class="btn btn-sm btn-primary mr-1">Детали</a>
                                            <form method="POST" action="{{ route('user.destroy', $user) }}">
                                                @csrf
                                                @method('DELETE')
                                                <button class="btn btn-sm btn-danger mr-1">Удалить</button>
                                            </form>
                                        </div>
                                    @endcan
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                    {{ $users->appends(request()->query())->links() }}
                </div>
            </div>
        </div>
    </div>

@endsection