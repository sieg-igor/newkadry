@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    Создание нового кандидата
                    <a href="{{ route('user.index') }}" class="close">
                        <span aria-hidden="true">&times;</span>
                    </a>
                </div>

                <div class="card-body">
                    <form method="POST" action="{{ route('user.store') }}" enctype="multipart/form-data">
                        @csrf

                        @include('user._user-create-form-fields')

                        <div class="form-group">
                            <label class="col-form-label">Нотификации через</label>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="notify_with[]" value="email" id="notify-email">
                                <label class="form-check-label" for="notify-email">
                                    Имейл
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="notify_with[]" value="slack" id="notify-slack">
                                <label class="form-check-label" for="notify-slack">
                                    Слак
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="notify_with[]" value="telegram" id="notify-telegram">
                                <label class="form-check-label" for="notify-telegram">
                                    Телеграм
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="notify_with[]" value="skype" id="notify-skype">
                                <label class="form-check-label" for="notify-skype">
                                    Скайп
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Сохранить</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection