<!DOCTYPE html>
<html>
<head>
    <title>Напоминане о собеседовании</title>
</head>

<body>
    <h2>Напоминание о собеседовании!</h2>
    <br/>
    Напоминание, на {{$interview->date_from}} в {{$interview->date_to}} состоится собеседование.
    <table class="table table-bordered table-striped">
        <tbody>
        <tr>
            <th>Полное имя</th><td>{{ $interview->user->first_name . ' ' . $interview->user->last_name . ' ' . $interview->user->second_name }}</td>
        </tr>
        <tr>
            <th>Город</th><td>{{ $interview->city->title }}</td>
        </tr>
        <tr>
            <th>Должность</th><td>{{ $interview->position->title }}</td>
        </tr>
        <tr>
            <th>Вакансия</th><td>{{ $interview->vacancy[0]->title }}</td>
        </tr>
        <tr>
            <th>Участники от нас</th>
            @foreach($interview->interviewEmployee as $interviewEmployee)
                <span class="badge badge-pill badge-light">
                    <i class="icon-remove interviewer-remove interviewer-{{$interviewEmployee->user_id or 'USER_ID_IS_NOT_DEFINED'}}" style="cursor:pointer;" data-interviewer_id="{{$interviewEmployee->user_id or 'USER_ID_IS_NOT_DEFINED'}}"></i>
                    {{ \App\Entities\User::getUserById($interviewEmployee->user_id) ?? '' }}
                </span>
            @endforeach
        </tr>
        <tr>
            <th>Телефон</th><td>{{ $interview->user->phone }}</td>
        </tr>
        <tr>
            <th>E-mail</th><td>{{ $interview->user->email }}</td>
        </tr>
        <tr>
            <th>Ссылки на соцсети</th><td>{{ $interview->user->socials }}</td>
        </tr>
        <tr>
            <th>Дата</th><td>{{ $interview->date_from }}</td>
        </tr>
        <tr>
            <th>Время</th><td>{{ $interview->date_to }}</td>
        </tr>
        <tr>
            <th>Фото</th>
            <td>
                @if ($interview->user->photo)
                    <img class="little-photo img-circle" src="{{ asset('storage/' . $interview->user->photo) }}" />
                @endif
            </td>
        </tr>
        </tbody>
    </table>
</body>

</html>