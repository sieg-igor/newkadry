<!DOCTYPE html>
<html>
<head>
    <title>Welcome Email</title>
</head>

<body>
    <h2>Hello {{$data->name}}!</h2>
    <br/>
    {{$the_message}}
    Dear {{$data->name}}, You have register at the  <a href="{{ url('/') }}">{{ config('app.name') }}</a>.
    Welcome to {{ config('app.name') }} :)
</body>

</html>