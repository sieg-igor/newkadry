@extends('layouts.app')

@section('content')

<?php /** @var bool $isCandidate */ ?>
<?php /** @var \App\Entities\User[] $candidates */ ?>

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Кандидаты
                    <a href="{{ route('home') }}" class="close">
                        <span aria-hidden="true">&times;</span>
                    </a>
                </div>

                <div class="card-body">
                    <form action="?" method="GET">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="name" class="col-form-label">Имя либо фамилия</label>
                                    <input id="name" class="form-control" name="name" value="{{ request('name') }}">
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label class="col-form-label">&nbsp;</label><br/>
                                    <button type="submit" class="btn btn-primary">Поиск</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    @can('manage-users')
                        <p><a href="{{ route('candidate.create') }}" class="btn btn-success">Добавить кандидата</a></p>
                    @endcan

                    <!-- v2 -->

                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Дата</th>
                            <th>Кандидат</th>
                            <th>Авка</th>
                            <th>HR</th>
                            <th>Примечания</th>
                            <th>Опции</th>
                            <th>Опции</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($candidates as $candidate)

                            <tr>
                                {{--<td>{{$user->candidate->updated_at}}</td>--}}
                                {{--<td><a href="{{ route('user.show', $user->id) }}">{{$user->first_name . ' ' . $user->last_name . ' ' . $user->second_name}}</a></td>--}}
                                {{--<td>{{$user->candidate->recruiter->first_name . ' ' . $user->candidate->recruiter->last_name}}</td>--}}
                                {{--<td>{{$user->candidate->position->title}}</td>--}}
                                {{--<td>{{$user->candidate->city->title}}</td>--}}
                                {{--<td>{{$user->candidate->salary . ' ' . $user->candidate->currency->title}}</td>--}}
                                {{--<td><span class="badge badge-primary">{{$statusesList[$user->candidate->status]}}</span></td>--}}
                                {{--<td>{{$user->candidate->notes}}</td>--}}
                                {{--<td>--}}
                                {{--@can('manage-users')--}}
                                {{--<div class="d-flex flex-row">--}}
                                {{--<a href="{{ route('user.show', $user) }}" class="btn btn-sm btn-primary mr-1">Детали</a>--}}
                                {{--<form method="POST" action="{{ route('user.destroy', $user) }}">--}}
                                {{--@csrf--}}
                                {{--@method('DELETE')--}}
                                {{--<button class="btn btn-sm btn-danger mr-1">Удалить</button>--}}
                                {{--</form>--}}
                                {{--</div>--}}
                                {{--@endcan--}}
                                {{--</td>--}}
                            </tr>
                            <!-- 22.06.18 12:51 -->
                            <tr>
                                <td>{{ $candidate['updated_at'] ?? '' }}</td>
                                <td>
                                    <a style="text-decoration: none"
                                       href="{{ route('user.show', $candidate['id'] ?? '' ) }}">
                                        {{  $candidate['first_name'] ?? 'first_name'  }}
                                        {{  $candidate['last_name'] ?? 'last_name'  }}
                                        {{  $candidate['second_name'] ?? 'second_name'  }}
                                        <br>
                                    </a>
                                        <span class="badge badge-success">{{ $candidate['candidate']['position']['title'] }}</span> &nbsp;
                                        <span class="badge badge-primary">{{ $candidate['candidate']['city']['title'] }}</span> &nbsp;
                                        <span class="badge badge-outline">{{ $candidate['candidate']['salary'] . ' ' . $candidate['candidate']['currency']['title'] }}</span> &nbsp;

                                </td>
                                <td>
                                    <a href="{{ route('user.show', $candidate['id']) }}">
                                        <img src="/storage/{{ $candidate['photo'] }}" alt="" width="50px"
                                             class="img rounded-circle">
                                    </a>
                                </td>
                                <td>{{ $candidate['candidate']['recruiter']['first_name']/* . ' ' . $candidate['candidate']['recruiter']['last_name']*/ }}</td>
                                <td>
                                    <!-- Is this candidate?  Look for his candidate::status-->
                                    @if($candidate['status'] == 3)
                                        <?php
                                        $isCandidate = true;
                                        switch ($candidate['candidate']['status']) {
                                            case -2:
                                                $class = 'outline'; // got offer
                                                break;
                                            case -1:
                                                $class = 'danger'; // fired
                                                break;
                                            case 1:
                                                $class = 'outline'; // dialog open
                                                break;
                                            case 4:
                                                $class = 'outline'; // denied
                                                break;
                                            case 5:
                                                $class = 'outline'; // refused
                                                break;
                                            default:
                                                $class = 'outline'; // interview
                                                break;
                                        }
                                        ?>
                                        <span class="badge badge-primary">
                                            Кандидат
                                        </span>
                                        <span class="badge badge-<?=$class ?? '' ?>">
                                                {{ $statusesList[$candidate['candidate']['status']] ?? "Undefined" }}
                                        </span>
                                    @endif
                                <!-- Is this NOT Candidate?  So this is a User and we look for his user::status -->
                                    @if($candidate['status'] != 3)
                                        <?php
                                        switch ($candidate['status']) {
                                            case -2:
                                                $class = 'outline'; // got offer
                                                break;
                                            case -1:
                                                $class = 'danger'; // fired
                                                break;
                                            case 1:
                                                $class = 'outline'; // dialog open
                                                break;
                                            case 4:
                                                $class = 'outline'; // denied
                                                break;
                                            case 5:
                                                $class = 'outline'; // refused
                                                break;
                                            default:
                                                $class = 'outline'; // interview
                                                break;
                                        }
                                        ?>
                                        <span class="badge badge-danger">
                                            Сотрудник
                                        </span>
                                        <span class="badge badge-<?=$class ?? '' ?>">
                                            {{ $statusesList[$candidate['status']] ?? "Undefined" }}
                                        </span>
                                    @endif
                                </td>
                                <td></td>
                                <td>
                                    @can('manage-users')
                                        <div class="d-flex flex-row">
                                            {{--<a href="{{ route('user.show', $user ?? '' ) }}" class="btn btn-sm btn-primary mr-1">Детали</a>--}}
                                            <form method="POST"
                                                  action="{{ route('candidate.destroy', null !== $candidate ?? 0 ) }}">
                                                @csrf
                                                @method('DELETE')
                                                <button class="btn btn-sm btn-danger mr-1">Удалить</button>
                                            </form>
                                        </div>
                                    @endcan
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection