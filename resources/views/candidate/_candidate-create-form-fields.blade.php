<div class="form-group">
    <label for="position" class="col-form-label required">Должность</label>
    <select id="position" class="form-control{{ $errors->has('position') ? ' is-invalid' : '' }}" name="position" required>
        <option value=""></option>
        @foreach ($data->positionsList as $key => $value)
            <option value="{{ $key }}"{{ $key == old('position') ? ' selected' : '' }}>
                {{ $value }}
            </option>
        @endforeach
    </select>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="salary" class="col-form-label">Зарплата</label>
            <input id="salary" class="form-control{{ $errors->has('salary') ? ' is-invalid' : '' }}" name="salary" value="{{ old('salary') }}">
            @if ($errors->has('salary'))
                <span class="invalid-feedback"><strong>{{ $errors->first('salary') }}</strong></span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="currency" class="col-form-label">Валюта</label>
            <select id="currency" class="form-control{{ $errors->has('currency') ? ' is-invalid' : '' }}" name="currency">
                @foreach ($data->currenciesList as $key => $value)
                    <option value="{{ $key }}"{{ $key == old('currency') ? ' selected' : '' }}>
                        {{ $value }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="city" class="col-form-label required">Город</label>
            <select id="city" class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}" name="city" required>
                @foreach ($data->citiesList as $key => $value)
                    <option value="{{ $key }}"{{ $key == old('city') ? ' selected' : '' }}>
                        {{ $value }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="company" class="col-form-label">Последнее место работы</label>
            <select id="company" class="form-control{{ $errors->has('company') ? ' is-invalid' : '' }}" name="company">
                <option value=""></option>
                @foreach ($data->companiesList as $key => $value)
                    <option value="{{ $key }}"{{ $key == old('company') ? ' selected' : '' }}>
                        {{ $value }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="cv_src" class="col-form-label">Ссылка на резюме</label>
            <input id="cv_src" class="form-control{{ $errors->has('cv_src') ? ' is-invalid' : '' }}" name="cv_src" value="{{ old('cv_src') }}">
            @if ($errors->has('cv_src'))
                <span class="invalid-feedback"><strong>{{ $errors->first('cv_src') }}</strong></span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="cv_file" class="col-form-label">Прикрепить резюме</label>
            <input id="cv_file" type="file" class="form-control{{ $errors->has('cv_file') ? ' is-invalid' : '' }}" name="cv_file">
            @if ($errors->has('cv_file'))
                <span class="invalid-feedback"><strong>{{ $errors->first('cv_file') }}</strong></span>
            @endif
        </div>
    </div>
</div>

<div class="form-group">
    <label for="languages" class="col-form-label">Языки</label>
    <textarea id="languages" class="form-control{{ $errors->has('languages') ? ' is-invalid' : '' }}" name="languages" rows="2">{{ old('languages') }}</textarea>
    @if ($errors->has('languages'))
        <span class="invalid-feedback"><strong>{{ $errors->first('languages') }}</strong></span>
    @endif
</div>

<div class="form-group">
    <label for="description" class="col-form-label">Описание</label>
    <textarea id="description" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" rows="2">{{ old('description') }}</textarea>
    @if ($errors->has('description'))
        <span class="invalid-feedback"><strong>{{ $errors->first('description') }}</strong></span>
    @endif
</div>

<div class="form-group">
    <label for="notes" class="col-form-label">Заметки</label>
    <textarea id="notes" class="form-control{{ $errors->has('notes') ? ' is-invalid' : '' }}" name="notes" rows="2">{{ old('notes') }}</textarea>
    @if ($errors->has('notes'))
        <span class="invalid-feedback"><strong>{{ $errors->first('notes') }}</strong></span>
    @endif
</div>