@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Кандидаты
                    <a href="{{ route('home') }}" class="close">
                        <span aria-hidden="true">&times;</span>
                    </a>
                </div>

                <div class="card-body">
                    <form action="?" method="GET">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="name" class="col-form-label">Имя либо фамилия</label>
                                    <input id="name" class="form-control" name="name" value="{{ request('name') }}">
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label class="col-form-label">&nbsp;</label><br />
                                    <button type="submit" class="btn btn-primary">Поиск</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    @can('manage-users')
                        <p><a href="{{ route('candidate.create') }}" class="btn btn-success">Добавить кандидата</a></p>
                    @endcan
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Дата</th>
                            <th>Кандидат</th>
                            <th>Авка</th>
                            <th>HR</th>
                            <th>Должность</th>
                            <th>Регион</th>
                            <th>Зарплата</th>
                            {{--<th>Статус</th>--}}
                            <th>Примечания</th>
                            <th>Опции</th>
                        </tr>
                        </thead>
                        <tbody>


                        @foreach ($candidates as $user)
                            <tr>
                                {{--<td>{{$user->candidate->updated_at}}</td>--}}
                                {{--<td><a href="{{ route('user.show', $user->id) }}">{{$user->first_name . ' ' . $user->last_name . ' ' . $user->second_name}}</a></td>--}}
                                {{--<td>{{$user->candidate->recruiter->first_name . ' ' . $user->candidate->recruiter->last_name}}</td>--}}
                                {{--<td>{{$user->candidate->position->title}}</td>--}}
                                {{--<td>{{$user->candidate->city->title}}</td>--}}
                                {{--<td>{{$user->candidate->salary . ' ' . $user->candidate->currency->title}}</td>--}}
                                {{--<td><span class="badge badge-primary">{{$statusesList[$user->candidate->status]}}</span></td>--}}
                                {{--<td>{{$user->candidate->notes}}</td>--}}
                                {{--<td>--}}
                                    {{--@can('manage-users')--}}
                                        {{--<div class="d-flex flex-row">--}}
                                            {{--<a href="{{ route('user.show', $user) }}" class="btn btn-sm btn-primary mr-1">Детали</a>--}}
                                            {{--<form method="POST" action="{{ route('user.destroy', $user) }}">--}}
                                                {{--@csrf--}}
                                                {{--@method('DELETE')--}}
                                                {{--<button class="btn btn-sm btn-danger mr-1">Удалить</button>--}}
                                            {{--</form>--}}
                                        {{--</div>--}}
                                    {{--@endcan--}}
                                {{--</td>--}}
                            </tr>
                            <!-- 22.06.18 12:51 -->
                            <tr>
                                <td>{{date('d.m.Y H:i', strtotime($user->candidate['created_at']))}}</td>
                                <td>
                                    <a style="text-decoration: none" href="{{ route('user.show', $user->id) }}">{{$user->first_name . ' ' . $user->second_name . ' ' . $user->last_name}}</a>
                                    <br>
                                    <?php
                                    switch ($user->candidate['status']){
                                        case 1:
                                            $class = 'success'; // dialog open
                                            break;
                                        case 4:
                                            $class = 'danger'; // denied
                                            break;
                                        case 5:
                                            $class = 'warning'; // refused
                                            break;
                                        default:
                                            $class = 'primary'; // interview
                                            break;
                                    }
                                    ?>
                                    <span class="badge badge-<?=$class?>">
                                        {{ $statusesList[$user->candidate['status']] ?? "Undefined" }}
                                    </span>
                                </td>
                                <td><a href="{{ route('user.show', $user->id) }}"><img src="/storage/{{$user['photo']}}" alt="" width="50px" class="img rounded-circle"></a></td>
                                <td>{{$user->candidate['recruiter']['first_name'] . ' ' . $user->candidate['recruiter']['last_name']}}</td>
                                <td>{{$user->candidate['position']['title']}}</td>
                                <td>{{$user->candidate['city']['title']}}</td>
                                <td>{{$user->candidate['salary'] . ' ' . $user->candidate['currency']['title']}}</td>

                                <td>{{$user->candidate['notes']}}</td>
                                <td>
                                @can('manage-users')
                                <div class="d-flex flex-row">
                                <a href="{{ route('user.show', $user) }}" class="btn btn-sm btn-primary mr-1">Детали</a>
                                <form method="POST" action="{{ route('user.destroy', $user) }}">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-sm btn-danger mr-1">Удалить</button>
                                </form>
                                </div>
                                @endcan
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                    {{ $candidates->links() }}
                </div>
            </div>
        </div>
    </div>

@endsection