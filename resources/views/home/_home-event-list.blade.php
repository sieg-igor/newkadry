<?php
echo '<pre>';
//print_r($users);
echo '</pre>';
?>
{{--@dd($interviews)--}}
@foreach($events as $event)
    <?php
        $type = $event['event_id'] ?? 0;
        $class = \App\Event::getColor($type) ?? '';
    ?>
    <div class="alert alert-{{ $class }}" role="alert">
        <div class="row">
            <div class="col-sm-2">
                {{  $event['timestamp'] or 'TIMESTAMP_IS_NOT_DEFINED'}}
            </div>

            <div class="col-sm-7">
<!-- ТИП -->        {{ $event_types[$event['event_id']] or 'EVENT_IS_NOT_DEFINED' }}<br><br>
<!-- КАНДИДАТ -->@if ($type == 2) <!-- new candidate added -->
                    {{ $users[$event['user_id']]["last_name"]  or 'LAST_NAME_IS_NOT_DEFINED' }}
                    {{ $users[$event['user_id']]["first_name"]  or 'FIRST_NAME_IS_NOT_DEFINED' }}
                    {{ $users[$event['user_id']]["second_name"]  or 'SECOND_NAME_IS_NOT_DEFINED' }}
                @endif
<!-- ИНТЕРВЬЮ -->@if ($type == 3)
                Кандидат:
                <a href="user/{{ $event['user_id'] or 'USER_ID_IS_NOT_DEFINED' }}" style="text-decoration: none; border-bottom: 1px dotted red; color: #c05858;">
                    {{ $users[$event['user_id']]["last_name"]  or 'LAST_NAME_IS_NOT_DEFINED' }}
                    {{ $users[$event['user_id']]["first_name"]  or 'FIRST_NAME_IS_NOT_DEFINED' }}
                    {{ $users[$event['user_id']]["second_name"]  or 'SECOND_NAME_IS_NOT_DEFINED' }}
                </a>
                <br>
                Позиция: {{$positions[$event['position_id']] or 'POSITION_IS_NOT_DEFINED' }}
                <br>
                Город: {{ $cities[$event['city_id']] or 'CITY_IS_NOT_DEFINED' }}
                <br>
                Время: {{date('d.m.Y', strtotime( $event->interview->date_from ?? '' )) }}
                в   {{ date('H:i', strtotime( $event->interview->date_from ?? '' )) }}
                @endif
<!-- СОТРУДНИК -->
                @if ($type == 1)
                <a href="user/{{$event['user_id'] or 'USER_ID_IS_NOT_DEFINED'}}" style="border-bottom: 1px dotted blue; text-decoration: none">
                    {{ $users[$event['user_id']]['last_name']  or 'LAST_NAME_IS_NOT_DEFINED' }}
                    {{ $users[$event['user_id']]['first_name']  or 'FIRST_NAME_IS_NOT_DEFINED' }}
                    {{ $users[$event['user_id']]['second_name']  or 'SECOND_NAME_IS_NOT_DEFINED' }}
{{--                    {{ $users[5]['second_name']  or 'SECOND_NAME_IS_NOT_DEFINED' }}--}}
                </a>
                @endif
<!-- ВАКАНСИЯ -->
                @if ($type == 6)
                <a href="vacancy/{{ $event['vacancy_id'] or 'VACANCY_IS_NOT_DEFINED'}}" style="text-decoration: none; border-bottom: 1px dotted blue;">
                    {{ $vacancies[$event['vacancy_id']] or 'VACANCY_TITLE_IS_NOT_DEFINED' }}
                </a>
                @endif
                </div>

                <div class="col-sm-3">
                @if ($type == 1) <!-- new employee added -->
                    <a href="user/{{$event['user_id'] or 'USER_ID_IS_NOT_DEFINED'}}" style="border-bottom: 1px dotted blue; text-decoration: none">
                        <img src="storage/{{ $users[$event['user_id']]['photo'] ?? 'PHOTO_IS_NOT_DEFINED'}}" alt="" class="img img-circle" width="75px">
                    </a>
                @endif
                @if ($type == 2) <!-- new candidate added -->
                    <a href="candidate" target="_blank">
                        <button class="btn btn-success btn-sm" style="text-align: right">Все кандидаты</button>
                    </a>
                @endif
                @if ($type == 3) <!-- new interview added -->
                    <a href="interview" target="_blank">
                        <button class="btn btn-danger btn-sm" style="text-align: right">Все собеседования</button>
                    </a>
                @endif
                @if ($type == 6)<!-- new vacancy added -->
                    <a href="vacancy" target="_blank">
                        <button class="btn btn-outline-warning btn-sm" style="text-align: right">Все вакансии</button>
                    </a>
                @endif
                @if ($type == 7) <!-- new position added -->
                    <a href="data/position" target="_blank">
                        <button class="btn btn-warning btn-sm" style="text-align: right">Все должности</button>
                    </a>
                @endif
            </div>
        </div>
    </div>
    <br><br>
@endforeach
{{ $events->links() }}
