<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ __('FaceIT HR') }}</title>

    <!-- Styles -->
    <link href="{{ mix('css/app.css', 'build') }}" rel="stylesheet">
    <!-- jQuery must loaded first -->
    <script src="{{ mix('js/jquery.min.js', 'build') }}"></script>
    @yield('styles')

</head>
<body>
    <div >
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ __('FaceIT HR') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                        @auth
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Статистика
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('candidate.stat') }}">{{ __('История найма')}}</a>
                                </div>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Календарь
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('calendar.index') }}">{{ __('Календарь')}}</a>
                                    <a class="dropdown-item" href="{{ route('calendar.add') }}">{{ __('Добавить событие')}}</a>
                                </div>
                            </li>
                            <li><a class="nav-link" href="{{ route('candidate.index') }}">{{ __('Кандидаты') }}</a></li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Сотрудники
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('user.index') }}">{{ __('Список наших сотрудников')}}</a>
                                    <a class="dropdown-item" href="{{ route('user.create') }}">{{ __('Добавить нового')}}</a>
                                </div>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Вакансии
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('vacancy.index') }}">{{ __('Открытые вакансии')}}</a>
                                    <a class="dropdown-item" href="{{ route('vacancy.create') }}">{{ __('Создать новую')}}</a>
                                </div>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Интервью
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('interview.index') }}">{{ __('Предстоящие')}}</a>
                                    <a class="dropdown-item" href="{{ route('interview.create') }}">{{ __('Создать новое')}}</a>
                                </div>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Данные
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('data.currency.index') }}">{{ __('Валюты')}}</a>
                                    <a class="dropdown-item" href="{{ route('data.company.index') }}">{{ __('Компании')}}</a>
                                    <a class="dropdown-item" href="{{ route('data.city.index') }}">{{ __('Города')}}</a>
                                    <a class="dropdown-item" href="{{ route('data.position.index') }}">{{ __('Должности')}}</a>
                                    <a class="dropdown-item" href="{{ route('data.remote-access.index') }}">{{ __('Доступы к сайтам')}}</a>
                                </div>
                            </li>
                        @endauth
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li><a class="nav-link" href="{{ route('login') }}">{{ __('Вход') }}</a></li>
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->email }} <span class="caret"></span>
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('user.show', Auth::user()) }}">{{ __('Профиль') }}</a>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Выход') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4 container">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif

            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif

                <div class="calendar"></div>

            @yield('content')
        </main>
    </div>
    {{--<script src="{!! asset('js/manifest.js') !!}"></script>--}}
    {{--<script src="{!! asset('js/vendor.js') !!}"></script>--}}
    <script src="{{ mix('js/app.js', 'build') }}"></script>

@yield('scripts')

<script>
    $(document).ready(function () {
        // alert('Page Loaded');
        $('.fc-body').find('fc-day').click(function () {
            alert('Clicked');
            let date = $(this).data('date');
            alert(date);
            // window.location.href = '/calendar/create?date='+date;
        })
    })
</script>


</body>
</html>
