<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => env('SES_REGION', 'us-east-1'),
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    'telegram-bot-api' => [
        'token' => env('INTERVIEW_NOTIFICATOR_TELEGRAM_TOKEN', 'YOUR BOT TOKEN HERE')
    ],
    'skype-bot-api' => [
        "app_id" => env('INTERVIEW_NOTIFICATOR_SKYPE_APP_ID', '' ),
        "app_secret" => env('INTERVIEW_NOTIFICATOR_SKYPE_SECRET', ''),
        "api_url" => env('INTERVIEW_NOTIFICATOR_SKYPE_API_URL', 'https://apis.skype.com/v2/conversations/'),
        "api_token_url" =>  env('INTERVIEW_NOTIFICATOR_SKYPE_API_TOKEN_URL', 'https://login.microsoftonline.com/botframework.com/oauth2/v2.0/token'),
        "grand_type" => env('INTERVIEW_NOTIFICATOR_SKYPE_GRAND_TYPE', 'client_credentials'),
        "scope" => env('INTERVIEW_NOTIFICATOR_SKYPE_SCOPE', 'https://api.botframework.com/.default'),
    ],
    'slack' => [
        'slack_webhook_url' => env('SLACK_WEBHOOK_URL', ''),
        'channel' => env('SLACK_DEFAULT_CHANNEL', '#general')
    ],
];
