<?php
return [
    'drivers'   => [
        'mail' => [
            "class" => \meatmanFS\Notificator\Lib\Drivers\Mail::class,
            "options" => [
                "template" => "notifier.email",
                /* something like default template and other options for ;this driver */
            ],
        ],
        'telegram' => [
            "class" => \meatmanFS\Notificator\Lib\Drivers\Telegram::class,
            "options" => [
                'template'  => 'notifier.message',
                'bot_token' => env('INTERVIEW_NOTIFICATOR_TELEGRAM_TOKEN', '' ),
                'api_url'   => env('INTERVIEW_NOTIFICATOR_TELEGRAM_API_URL', 'https://api.telegram.org/bot' ),
            ],
        ],
        'skype'     => [
            "class" =>  \meatmanFS\Notificator\Lib\Drivers\Skype::class,
            "options" => [
                'template'  => 'notifier.message',
                "app_id" => env('INTERVIEW_NOTIFICATOR_SKYPE_APP_ID', '' ),
                "app_secret" => env('INTERVIEW_NOTIFICATOR_SKYPE_SECRET', ''),
                "api_url" => env('INTERVIEW_NOTIFICATOR_SKYPE_API_URL', 'https://apis.skype.com/v2/conversations/'),
                "api_token_url" =>  env('INTERVIEW_NOTIFICATOR_SKYPE_API_TOKEN_URL', 'https://login.microsoftonline.com/botframework.com/oauth2/v2.0/token'),
                "grand_type" => env('INTERVIEW_NOTIFICATOR_SKYPE_GRAND_TYPE', 'client_credentials'),
                "scope" => env('INTERVIEW_NOTIFICATOR_SKYPE_SCOPE', 'https://api.botframework.com/.default'),
            ],
        ],
        'slack'    => [
            'class' => \meatmanFS\Notificator\Lib\Drivers\Slack::class,
            'options' => [
                'template'  => 'notifier.message',
                'slack_webhook_url' => env('SLACK_WEBHOOK_URL', ''),
                'default_channel'   => env('SlACK_DEFAULT_CHANNEL','#general'),
            ],
        ],
    ],
];
