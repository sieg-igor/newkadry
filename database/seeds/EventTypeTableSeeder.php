<?php

use App\Entities\Data\EventType;
use Illuminate\Database\Seeder;

class EventTypeTableSeeder extends Seeder
{
    protected $event_type;

    public function __construct()
    {
        $this->event_type = [
            [
                'id' => '1',
                'title'  => 'Добавлен новый сотрудник'
            ],
            [
                'id' => '2',
                'title' => 'Добавлен новый кандидат'
            ],
            [
                'id' => '3',
                'title' => 'Добавлено новое собеседование'
            ],
            [
                'id' => '4',
                'title' => 'Добавлен новый интервьювер'
            ],
            [
                'id' => '5',
                'title' => 'Добавлен новый город'
            ],
            [
                'id' => '6',
                'title' => 'Добавлена новая вакансия'
            ],
            [
                'id' => '7',
                'title' => 'Добавлена новая должность'
            ],
        ];
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EventType::insert($this->event_type);
    }
}
