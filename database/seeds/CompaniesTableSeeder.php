<?php

use App\Entities\Data\Company;
use Illuminate\Database\Seeder;

class CompaniesTableSeeder extends Seeder
{
    protected $companies;

    public function __construct()
    {
        $this->companies = [
            [
                'title' => 'FaceIT',
                'city_id' => 1,
                'about' => 'FaceIT'
            ],
            [
                'title' => 'GBKSoft',
                'city_id' => 1,
                'about' => 'GBKSoft'
            ],
            [
                'title' => 'FreeUA',
                'city_id' => 1,
                'about' => 'FreeUA'
            ],
        ];
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Company::insert($this->companies);
    }
}
