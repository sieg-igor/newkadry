<?php

use App\Entities\Data\Vacancy;
use Illuminate\Database\Seeder;

class VacanciesTableSeeder extends Seeder
{

    protected $vacancies;

    public function __construct()
    {
        $this->vacancies = [
            [
                'title' => 'PHP-разработчик',
                'description' => 'Описание',
                'salary' => 250,
                'currency_id' => 2,
                'city_id' => 1,
                'user_id' => 10
            ],
            [
                'title' => 'Ruby-разработчик',
                'description' => 'Описание',
                'salary' => 300,
                'currency_id' => 2,
                'city_id' => 1,
                'user_id' => 11
            ],
            [
                'title' => 'JS-разработчик',
                'description' => 'Описание',
                'salary' => 400,
                'currency_id' => 2,
                'city_id' => 1,
                'user_id' => 12
            ],
            [
                'title' => 'iOS-разработчик',
                'description' => 'Описание',
                'salary' => 700,
                'currency_id' => 2,
                'city_id' => 2,
                'user_id' => 13
            ],
        ];
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Vacancy::insert($this->vacancies);
    }
}
