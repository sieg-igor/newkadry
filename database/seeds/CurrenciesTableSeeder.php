<?php

use App\Entities\Data\Currency;
use Illuminate\Database\Seeder;

class CurrenciesTableSeeder extends Seeder
{
    protected $currencies;

    public function __construct()
    {
        $this->currencies = [
            ['title' => 'UAH'],
            ['title' => 'USD'],
            ['title' => 'По договоренности'],
        ];
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Currency::insert($this->currencies);
    }
}
