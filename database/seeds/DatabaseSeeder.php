<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

//        $this->call(BaseValueSeeder::class);

        $this->call(UsersTableSeeder::class);
        $this->call(CitiesTableSeeder::class);
        $this->call(CompaniesTableSeeder::class);
        $this->call(CurrenciesTableSeeder::class);
        $this->call(EventTypeTableSeeder::class);
        $this->call(PositionsTableSeeder::class);
        $this->call(VacanciesTableSeeder::class);
    }
}
