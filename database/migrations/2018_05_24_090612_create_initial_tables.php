<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateInitialTables
 */
class CreateInitialTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('positions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 40);
        });

        Schema::create('currencies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 40);
        });

        Schema::create('cities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 40);
        });

        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 40);
            $table->integer('city_id')->unsigned()->nullable();
            $table->text('about');

            $table->foreign('city_id')->references('id')->on('cities')->onDelete('SET NULL');
        });



        /** table for candidates history */
        Schema::create('candidates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('recruiter_id')->unsigned()->nullable();
            $table->string('status', 16);
            $table->integer('position_id')->unsigned()->nullable();
            $table->integer('salary')->nullable();
            $table->integer('currency_id')->unsigned()->nullable();
            $table->integer('city_id')->unsigned()->nullable();
            $table->string('cv_src', 255)->nullable();
            $table->text('languages')->nullable();
            $table->text('description')->nullable();
            $table->text('notes')->nullable();
            $table->string('cv_file', 255)->nullable();
            $table->integer('last_company_id')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('CASCADE');
            $table->foreign('recruiter_id')->references('id')->on('users')->onDelete('SET NULL');
            $table->foreign('position_id')->references('id')->on('positions')->onDelete('SET NULL');
            $table->foreign('currency_id')->references('id')->on('currencies')->onDelete('SET NULL');
            $table->foreign('city_id')->references('id')->on('cities')->onDelete('SET NULL');
            $table->foreign('last_company_id')->references('id')->on('companies')->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('candidates', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
            $table->dropForeign(['recruiter_id']);
            $table->dropForeign(['position_id']);
            $table->dropForeign(['currency_id']);
            $table->dropForeign(['city_id']);
            $table->dropForeign(['last_company_id']);
        });
        Schema::dropIfExists('candidates');

        Schema::table('companies', function (Blueprint $table) {
            $table->dropForeign(['city_id']);
        });
        Schema::dropIfExists('companies');

        Schema::dropIfExists('cities');
        Schema::dropIfExists('currencies');
        Schema::dropIfExists('positions');
    }
}
