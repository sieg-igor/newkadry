<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::group([
    'middleware' => ['auth', 'can:use-site'],
], function () {

/**
 * HOME &EVENT
 */
Route::get('/', 'HomeController@index')->name('home');
Route::get('/candidates', 'Candidate\CandidateController@getListMatch');
Route::get('/candidates-for-interview', 'Candidate\CandidateController@getCandidatesForInterview');
    Route::get('candidate/stat','Candidate\CandidateController@stat')->name('candidate.stat');

Route::resource('candidate', 'Candidate\CandidateController');
Route::group([
        'prefix' => 'candidate',
        'as' => 'candidate.',
        'namespace' => 'Candidate',
    ],
    function () {

        Route::post('change-status/{candidate}/{status}', 'CandidateController@changeStatus')->name('changeStatus');
        Route::get('create-candidate-record/{user}', 'CandidateController@createCandidateRecord')->name('createCandidateRecord');
        Route::post('store-candidate-record/{user}', 'CandidateController@storeCandidateRecord')->name('storeCandidateRecord');
        Route::get('get', 'CandidateController@getList');
        Route::get('candidates', 'CandidateController@getListMatch');
    }
);

Route::resource('user', 'User\UserController');
Route::group([
    'prefix' => 'user',
    'as' => 'user.',
    'namespace' => 'User',
],
    function () {
        Route::post('change-status/{user}/{status}', 'UserController@changeStatus')->name('changeStatus');
    }
);

Route::resource('vacancy', 'VacancyController');

Route::group([
    'prefix' => 'data',
    'as' => 'data.',
    'namespace' => 'Data',
],
    function () {
        Route::resource('currency', 'CurrencyController');
        Route::resource('city', 'CityController');
        Route::resource('position', 'PositionController');
        Route::resource('company', 'CompanyController');
        Route::resource('remote-access', 'RemoteAccessController');
    }
);

/**
 * INTERVIEWS
 */
Route::resource('interview', 'InterviewController');
Route::get('interview/candidates/all', 'InterviewController@getCandidates')->name('interview.candidates.getAll');


/**
 * EVENTS & CALENDAR
 */
Route::any('calendar', 'Event\EventController@index')->name('calendar.index');
Route::any('calendar/create', 'Event\EventController@create')->name('calendar.create');
Route::any('calendar/add', 'Event\EventController@add')->name('calendar.add');

});



/**
 * USERS
 */

